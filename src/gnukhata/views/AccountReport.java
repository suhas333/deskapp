package gnukhata.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.TabExpander;

import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.reportmodels.accountReport;
import gnukhata.controllers.reportmodels.extendedTrialBalance;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.Style;
import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import sun.security.provider.PolicyParser.GrantEntry;

public class AccountReport extends Composite {
	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	
	String strOrgName;
	String strFromYear;
	String strToYear;
	String accname;
	
	String strFromDate;
	String strToDate;
	
	Label lblLogo;
	Label lblOrgDetails;
	Label lblLine;
	Label lblheadline;
	Label lblOrgDetails1;
	Label lblListOfAccount;
	
	Label lblSrNo;
	Label lblAccountName;
	Label lblGroupname;
	Label lblSubGroupName;
	
	Button btnPrint;
	
	TableViewer AccountList;
	
	int shellwidth = 0;
	int finshellwidth;
	
	static Display display;
	
	
	ODPackage  sheetStream;

	public AccountReport(Composite Parent, int Style, ArrayList<accountReport> accdata) {
		// TODO Auto-generated constructor stub
		super(Parent,Style);
		
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		
		MainShell.lblLogo.setVisible(false);
		MainShell.lblLine.setVisible(false);
		MainShell.lblOrgDetails.setVisible(false);
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout =new FormData();
	    /*strFromDate=frmDate.substring(8)+"-"+frmDate.substring(5,7)+"-"+frmDate.substring(0,4);
		strToDate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0,4);*/
	  
		//Label lblLogo = new Label(this, SWT.None);
		//layout = new FormData();
		//layout.top = new FormAttachment(1);
		//layout.left = new FormAttachment(63);
		//layout.right = new FormAttachment(87);
		//layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		//lblLogo.setLocation(getClientArea().width, getClientArea().height);
		//lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		//lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1] + "");
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails.setLayoutData(layout);
		
		Label lblOrgDetails1 = new Label(this,SWT.NONE);
		lblOrgDetails1.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails1.setText("For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails1.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(5);
		lblLine.setLayoutData(layout);
		
		
		lblListOfAccount=new Label(this, SWT.NONE);
		lblListOfAccount.setText("List Of Accounts");
		lblListOfAccount.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(45);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(8);
		lblListOfAccount.setLayoutData(layout);
		
		AccountList=new TableViewer(this, SWT.MULTI|SWT.BORDER|SWT.FULL_SELECTION|SWT.LINE_SOLID);
		AccountList.getTable().setLinesVisible (true);
		AccountList.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(lblListOfAccount, 8);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(92);
		AccountList.getTable().setLayoutData(layout);
		
		btnPrint=new Button(this, SWT.PUSH);
		btnPrint.setText("&Print");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(AccountList.getTable(), 10);
		layout.left=new FormAttachment(42);
		btnPrint.setLayoutData(layout);
		
		
		//this.makeaccssible(AccountList); 
		this.getAccessible();
		//this.pack();
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		shellwidth = this.getClientArea().width;
		finshellwidth = shellwidth-(shellwidth*2/100);
		
		try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/AccountReport.ots"),"AccountReport");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		this.setReport(accdata);
		this.setEvent(accdata);
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);

		globals.setThemeColor(this, Background, Foreground);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);

		AccountList.getTable().setBackground(FocusBackground);
		AccountList.getTable().setForeground(FocusForeground);


	}
	
	
	private void setReport(ArrayList<accountReport> accdata)
	{
		
		TableViewerColumn colSrNo = new TableViewerColumn(AccountList,SWT.None);
		colSrNo.getColumn().setText("Sr.No.");
		colSrNo.getColumn().setAlignment(SWT.LEFT);
		colSrNo.getColumn().setWidth(5 * finshellwidth /100);
		colSrNo.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.accountReport srNo = (accountReport) element;
		return srNo.getSrNo();
		}	
		});
		
		TableViewerColumn colaccname = new TableViewerColumn(AccountList, SWT.None);
		colaccname.getColumn().setText("                Account Name");
		colaccname.getColumn().setWidth(32 * finshellwidth /100);
		colaccname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.accountReport accname = (accountReport) element;
				return accname.getAccountName();
				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn colgrpname = new TableViewerColumn(AccountList, SWT.None);
		colgrpname.getColumn().setText("                 Group Name");
		colgrpname.getColumn().setAlignment(SWT.LEFT);
		colgrpname.getColumn().setWidth(30 * finshellwidth /100);
		colgrpname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.accountReport grpname = (accountReport) element;
				return grpname.getGroupName();
				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn colsubgrpname = new TableViewerColumn(AccountList, SWT.None);
		colsubgrpname.getColumn().setText("                 Sub-Group Name");
		colsubgrpname.getColumn().setAlignment(SWT.LEFT);
		colsubgrpname.getColumn().setWidth(24 * finshellwidth /100);
		colsubgrpname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.accountReport subgrpname = (accountReport) element;
				return subgrpname.getSubgroupName();
				//return super.getText(element);
			}
		});
		AccountList.setContentProvider(new ArrayContentProvider());
		AccountList.setInput(accdata);
		AccountList.getTable().pack();
		AccountList.getTable().setFocus();
		AccountList.getTable().setSelection(0);
			}
	
	private void setEvent(final ArrayList<accountReport> accdata)
	{
		btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				//String[] strOrgName=(String[])btnPrint.getData("orgname");
				//String[] strPrintCol=(String[])btnPrint.getData("printcolumns");
				
				try 
				{	
					/*MessageBox msg=new MessageBox(new Shell(),SWT.OK);
					msg.setMessage("print");
					msg.open();*/
					final File AccountReport = new File("/tmp/gnukhata/Report_Output/AccountReport" );
					
					final Sheet accountReportSheet =  sheetStream.getSpreadSheet().getFirstSheet();
					accountReportSheet.ensureRowCount(100000);
					accountReportSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
					accountReportSheet.getCellAt(0, 1).setValue("List Of Accounts");
					for(int rowcounter = 0; rowcounter < accdata.size(); rowcounter ++ )
					{
						//Object[] printRow = (Object[]) accData.get(rowcounter);
						accountReportSheet.getCellAt(0,rowcounter +3).setValue(accdata.get(rowcounter).getSrNo());
						accountReportSheet.getCellAt(1,rowcounter +3).setValue(accdata.get(rowcounter).getAccountName());
						accountReportSheet.getCellAt(2,rowcounter +3).setValue(accdata.get(rowcounter).getGroupName());
						accountReportSheet.getCellAt(3,rowcounter +3).setValue(accdata.get(rowcounter).getSubgroupName());
						
					}
					OOUtils.open(accountReportSheet.getSpreadSheet().saveAs(AccountReport));
					//OOUtils.open(AccountReport);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}

}
