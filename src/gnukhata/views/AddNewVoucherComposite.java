
package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.transactionController;
import gnukhata.controllers.reportmodels.AddVoucher;
import gnukhata.controllers.reportmodels.accountReport;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.security.auth.login.AccountLockedException;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.CellEditor.LayoutData;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.sun.corba.se.impl.protocol.giopmsgheaders.Message;
import com.sun.org.apache.xerces.internal.impl.dtd.models.CMAny;

//import com.sun.org.apache.bcel.internal.generic.Select;
//import com.sun.xml.internal.ws.encoding.SwACodec;

public class AddNewVoucherComposite extends Composite {
	boolean DecimalFlag = false;
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	String crdrcombofocus = "";
	int selectedIndex;
	boolean alterFlag = false;
	//ArrayList<String> DrSelectedAccounts = new ArrayList<String>();
	//ArrayList<String> CrSelectedAccounts = new ArrayList<String>();
	ArrayList<String> SelectedAccounts = new ArrayList<String>(); 
	long searchTexttimeout = 0;
	String searchText;
	Rectangle grpBounds;
	static Display display;
	double totalDrAmount = 0.00;
	double totalCrAmount = 0.00;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	List<String> masterQueryParams = new ArrayList<String>();
	List<Object> detailQueryParams = new ArrayList<Object>();
	//Label lbldate;
	Text txtddate;
	Label dash1;
	Text txtmdate;
	Label dash2;
	Text txtyrdate;
	public static Label lblsavemsg;
	Label lblvoucherno;
	Text txtvoucherno;
	

	
	
	Label addvoucher;
	Button btnsave;
	Button btnAddAccount;
	Label lblselprj;
	Combo comboselprj;
	Label lblnarration;
	Text txtnarration;
	NumberFormat nf;
	//int rowIndex = 2;
	Group grpVoucher;
	Group grpLabel;
	//boolean totalRowCalled = false;
	int grpVoucherHeight = 0;
		int grpVoucherWidth = 0;
	String typeFlag;
	Label lbldate;
	TableViewer tblVoucherView;
	TableViewerColumn colAccountName;
	TableViewerColumn colDrAmount;
	TableViewerColumn colCrAmount;
	ArrayList<AddVoucher>  lstVoucherRows;
	//Label lblCrDr;
	Combo cmbCr_Dr;
	Label lblAccounts;
	CCombo cmbAccounts;
	Label lblCrDrAmount;
	Text txtCrDrAmount;
	Label totalcr_dr;
	Label totalDr;
	Text txttotalDr;
	Label totalCr;
    Text txttotalCr;
    Color bgbtnColor;
    Color fgbtnColor;
    Color bgcomboColor;
    Color fgcomboColor;
    Color bgtxtColor;
    Color fgtxtColor;
    Color fgtblColor;
    Color bgtblColor;
    boolean focusflag = false;
    Group grpEntry;
	AddNewVoucherComposite(Composite parent, int Style, String voucherType) {
		
		super(parent, Style);
		//this.setExpandVertical(true);
		typeFlag = voucherType;
		Date today = new Date();
		String strToday = sdf.format(today);
		nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		
		
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		// this.setLayout(new FormLayout());
		 MainShell.lblLogo.setVisible(false);
		 MainShell.lblLine.setVisible(false);
		 MainShell.lblOrgDetails.setVisible(false);
		    

		addvoucher = new Label(this, SWT.NONE);
		addvoucher.setText(  typeFlag );
		addvoucher.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC));
		//addvoucher.setLayout(new  FormLayout());
		FormData layout = new FormData();
		layout.left = new FormAttachment(10);
		addvoucher.setLayoutData(layout);
		String voucherno = transactionController.getLastReference(typeFlag);
		
		lblvoucherno = new Label(this, SWT.NONE);
		lblvoucherno.setText("&Voucher No *");
		lblvoucherno.setFont(new Font(display, "Time New Roman", 10, SWT.NORMAL));
		layout = new FormData();
		//layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(addvoucher,70);
		//layout.right = new FormAttachment(15);
		/*layout.bottom = new FormAttachment(15);*/
		lblvoucherno.setLayoutData(layout);
		
	
		txtvoucherno = new Text(this, SWT.BORDER);
		txtvoucherno.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout = new FormData();
		//layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(lblvoucherno,5);
		layout.right = new FormAttachment(40);
		//layout.bottom = new FormAttachment(12);
		txtvoucherno.setLayoutData(layout);
		txtvoucherno.setEditable(true);
		txtvoucherno.setText(voucherno);
		txtvoucherno.clearSelection();
		txtvoucherno.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
		txtvoucherno.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
		txtvoucherno.setSelection(0, 2);
		
		
		lbldate = new Label(this, SWT.NONE);
		lbldate.setText("&Date :");
		lbldate.setFont(new Font(display, "Time New Roman", 10, SWT.NORMAL));
		layout = new FormData();
		//layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(txtvoucherno,100);
		//layout.right = new FormAttachment(50);
		/*layout.bottom = new FormAttachment(12)*/;
		lbldate.setLayoutData(layout);
		String lastDate = transactionController.getLastDate(typeFlag);
		
		
		
		txtddate = new Text(this, SWT.BORDER);
		txtddate.setTextLimit(2);
		txtddate.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout = new FormData();
		//layout.top = new FormAttachment();
		layout.left = new FormAttachment(lbldate,5);
		//layout.right = new FormAttachment(49);
		//layout.bottom = new FormAttachment(12);
		txtddate.setLayoutData(layout);
		txtddate.setEditable(true);
		txtddate.setText(lastDate.substring(8 ));
		txtddate.setSelection(0, 2);

		dash1 = new Label(this, SWT.NONE);
		dash1.setText("-");
		dash1.setFont(new Font(display, "Time New Roman", 14, SWT.NORMAL));
		layout = new FormData();
		//layout.top = new FormAttachment(addvoucher);
		layout.left = new FormAttachment(txtddate,5);
		//layout.right = new FormAttachment(8);
		//layout.bottom = new FormAttachment(10);
		dash1.setLayoutData(layout);
		
		txtmdate = new Text(this, SWT.BORDER);
		txtmdate.setTextLimit(2);
		txtmdate.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout = new FormData();
		//layout.top = new FormAttachment(addvoucher);
		layout.left = new FormAttachment(dash1,5);
		//layout.right = new FormAttachment(60);
		//layout.bottom = new FormAttachment(12);
		txtmdate.setLayoutData(layout);
		txtmdate.setEditable(true);
		txtmdate.setText(lastDate.substring(5,7));
		txtmdate.setSelection(0, 2);

		dash2 = new Label(this, SWT.NONE);
		dash2.setText("-");
		dash2.setFont(new Font(display, "Time New Roman", 14, SWT.NORMAL));
		layout = new FormData();
		//layout.top = new FormAttachment(addvoucher);
		layout.left = new FormAttachment(txtmdate,5);
		//layout.right = new FormAttachment(61);
		//layout.bottom = new FormAttachment(10);
		dash2.setLayoutData(layout);
		
		txtyrdate = new Text(this, SWT.BORDER);
		txtyrdate.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout = new FormData();
		//layout.top = new FormAttachment(addvoucher);
		layout.left = new FormAttachment(dash2,5);
		//layout.right = new FormAttachment(66);
		//layout.bottom = new FormAttachment(12);
		txtyrdate.setTextLimit(4);
		txtyrdate.setLayoutData(layout);
		txtyrdate.setEditable(true);
		txtyrdate.setText(lastDate.substring(0,4));
		txtyrdate.setSelection(0, 4);
		
		lblsavemsg = new Label(this, SWT.NONE);
		//lblsavemsg.setText("Voucher saved with voucher number " );
		lblsavemsg.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD|SWT.COLOR_RED));
		layout = new FormData();
		layout.top = new FormAttachment(82);
		layout.left = new FormAttachment(42);
		layout.right = new FormAttachment(82);
		layout.bottom = new FormAttachment(86);
		lblsavemsg.setLayoutData(layout);
		
	
		grpVoucher = new Group(this, SWT.BORDER | SWT.V_SCROLL);
		layout = new FormData();
		layout.top = new FormAttachment(lblvoucherno,14);
		layout.left = new FormAttachment(5);
		layout.right = new FormAttachment(95);
		layout.bottom = new FormAttachment(71);
		

		//voucherTable.setLayoutData(layout);
		
		grpVoucher.setLayoutData(layout);
		grpVoucher.setLayout(new FormLayout());
		//grpVoucher.getVerticalBar().setVisible(true);
		//grpVoucher.pack(); 
		//grpVoucher.setText("add "+ voucherType );
		
		grpEntry = new Group(this, SWT.BORDER);
		FormData  entry = new FormData();
		entry.left = new FormAttachment(5);
		entry.right = new FormAttachment(95);
		entry.top =  new FormAttachment(grpVoucher, 5);
		entry.bottom = new FormAttachment(80);
		grpEntry.setLayoutData(entry);
		grpEntry.setLayout(new FormLayout());
		
		

		
		
		lblselprj = new Label(this, SWT.NONE);
		lblselprj.setText("S&elect Project : ");
		lblselprj.setFont(new Font(display, "Time New Roman", 10, SWT.RIGHT));
		layout = new FormData();
		layout.top = new FormAttachment(grpEntry, 5);
		layout.left = new FormAttachment(5);
		//layout.right = new FormAttachment(20);
		layout.bottom = new FormAttachment(85);
		lblselprj.setLayoutData(layout);
		lblselprj.setVisible(true);
		
		
		comboselprj= new Combo(this, SWT.BORDER| SWT.READ_ONLY);
		comboselprj.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout=new FormData();
		layout.top=new FormAttachment(grpEntry, 5);
		layout.left=new FormAttachment(lblselprj);
		layout.right=new FormAttachment(40);
		layout.bottom=new FormAttachment(85);
		comboselprj.setLayoutData(layout);
		comboselprj.setVisible(true);
		comboselprj.add("No Project");
		if(comboselprj.getSelectionIndex()==-1)
		{
					comboselprj.setVisible(false);
					lblselprj.setVisible(false);
		}
		
		String[] allProjects = gnukhata.controllers.transactionController.getAllProjects();
		for (int i = 0; i < allProjects.length; i++ )
		{
			comboselprj.setVisible(true);
			lblselprj.setVisible(true);
			comboselprj.add(allProjects[i]);
		}
		comboselprj.select(0);
			
		// Narration
		lblnarration = new Label(this, SWT.NONE);
		lblnarration.setText("Narrat&ion     : ");
		lblnarration.setFont(new Font(display, "Time New Roman", 10, SWT.RIGHT));
		layout = new FormData();
		layout.top = new FormAttachment(88);
		layout.left = new FormAttachment(5);
		//layout.right = new FormAttachment(61);
		layout.bottom = new FormAttachment(94);
		lblnarration.setLayoutData(layout);
		
		txtnarration = new Text(this, SWT.MULTI | SWT.BORDER|SWT.WRAP);
		layout = new FormData();
		layout.top = new FormAttachment( 88);
		layout.left = new FormAttachment(lblnarration);
		layout.right = new FormAttachment(85);
		layout.bottom = new FormAttachment(94);
		txtnarration.setLayoutData(layout);

		
		
		// txtddate.setFocus();
		
		btnAddAccount = new Button(this, SWT.PUSH);
		btnAddAccount.setText("Add Acc&ount");
		
		btnAddAccount.setFont(new Font(display, "Time New Roman", 10, SWT.RIGHT));
		layout = new FormData();
		layout.top = new FormAttachment(95);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(40);
		//layout.bottom = new FormAttachment(95);
		btnAddAccount.setLayoutData(layout);
		
		

		btnsave = new Button(this, SWT.PUSH);
		btnsave.setText("&save");
		btnsave.setFont(new Font(display, "Time New Roman", 10, SWT.RIGHT));
		btnsave.setToolTipText("Click here to save the voucher.");
		layout = new FormData();
		layout.top = new FormAttachment(95);
		layout.left = new FormAttachment(45);
		layout.right = new FormAttachment(80);
		//layout.bottom = new FormAttachment(95);
		btnsave.setLayoutData(layout);
			
		this.getAccessible();
		
		
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		
			grpVoucherWidth = grpVoucher.getClientArea().width;
		grpVoucherHeight = grpVoucher.getClientArea().height;
		tblVoucherView = new TableViewer(grpVoucher, SWT.V_SCROLL| SWT.BORDER);
		FormData grpData = new FormData();
		grpData.top = new FormAttachment(0);
		grpData.left = new FormAttachment(0);
		grpData.right = new FormAttachment(100);
		grpData.bottom =  new FormAttachment(90);
		tblVoucherView.getTable().setLayoutData(grpData);
		tblVoucherView.getTable().setHeaderVisible(true);
		tblVoucherView.getTable().setLinesVisible(true);
		tblVoucherView.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
		tblVoucherView.getTable().setEnabled(false);
		lstVoucherRows = new ArrayList<AddVoucher>();
		colAccountName = new TableViewerColumn(tblVoucherView,SWT.NONE );
		colAccountName.getColumn().setWidth(47 * grpVoucherWidth/ 100 );
		colAccountName.getColumn().setText("Account Name ");
		colAccountName.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				AddVoucher row = (AddVoucher) element;
				return row.getAccountName();
				
				//return super.getText(element);
			}
		}
		
				);
		colDrAmount= new TableViewerColumn(tblVoucherView,SWT.NONE );
		colDrAmount.getColumn().setWidth(24 * grpVoucherWidth/ 100 );
		colDrAmount.getColumn().setText("Debit Amount ");
		colDrAmount.getColumn().setAlignment(SWT.RIGHT);
		colDrAmount.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				AddVoucher row = (AddVoucher) element;
				NumberFormat nf = NumberFormat.getInstance();
				nf.setGroupingUsed(false);
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				
				return nf.format(row.getDrAmount());
				
				//return super.getText(element);
			}
		}
		
				);
		colCrAmount = new TableViewerColumn(tblVoucherView,SWT.NONE );
		colCrAmount.getColumn().setWidth(20* grpVoucherWidth/ 100 );
		colCrAmount.getColumn().setText("Credit Amount ");
		colCrAmount.getColumn().setAlignment(SWT.RIGHT);
		colCrAmount.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				AddVoucher row = (AddVoucher) element;
				NumberFormat nf = NumberFormat.getInstance();
				nf.setGroupingUsed(false);
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				return nf.format(row.getCrAmount());
				
				//return super.getText(element);
			}
		}
		
				);
		tblVoucherView.setContentProvider(new ArrayContentProvider());
		tblVoucherView.setInput(lstVoucherRows);
        grpData = new FormData();
		grpData.top = new FormAttachment(82);
		grpData.left = new FormAttachment(0);
		grpData.right=new FormAttachment(10);
		
		/*lblCrDr = new Label(grpEntry, SWT.NONE);
		lblCrDr.setText("Dr&/Cr");
		grpData = new FormData();
		grpData.top = new FormAttachment(20);
		grpData.left = new FormAttachment(0);
		grpData.bottom = new FormAttachment(100);
		//grpData.right=new FormAttachment(10);
		lblCrDr.setLayoutData(grpData);*/
		
		cmbCr_Dr = new Combo(grpEntry, SWT.BORDER | SWT.READ_ONLY);
		cmbCr_Dr.setFont(new Font(display, "Times New Roman", 11, SWT.NORMAL));
		grpData= new FormData();
		grpData.top = new FormAttachment(10);
		grpData.left = new FormAttachment(0);
		grpData.bottom = new FormAttachment(90);
		//layout.right = new FormAttachment(84);
		//layout.bottom = new FormAttachment(57);
		cmbCr_Dr.add("Dr");
		cmbCr_Dr.select(0);	
		//cmbCr_Dr.setLayoutData(grpData);

		cmbCr_Dr.setLayoutData(grpData);
		//drpdwntrialbal.select(0);

		cmbCr_Dr.setEnabled(false);
		
		

		lblAccounts = new Label(grpEntry, SWT.NONE);
		lblAccounts.setText("A&ccount Name");
		grpData = new FormData();
		grpData.top = new FormAttachment(20);
		grpData.left = new FormAttachment(25);
		grpData.bottom = new FormAttachment(100);
		//grpData.right=new FormAttachment(10);
		lblAccounts.setLayoutData(grpData);
		
		cmbAccounts = new CCombo(grpEntry, SWT.BORDER | SWT.READ_ONLY	);
		cmbAccounts.setFont(new Font(display, "Times New Roman", 11, SWT.NORMAL));
		grpData= new FormData();
		grpData.top = new FormAttachment(10);
		grpData.left = new FormAttachment(lblAccounts,8);
		grpData.bottom = new FormAttachment(90);
		grpData.right = new FormAttachment(70);
		//layout.right = new FormAttachment(8);
		//layout.bottom = new FormAttachment(57);
		
		//cmbCr_Dr.setLayoutData(grpData);
	
		cmbAccounts.add("----------------------------Please Select-------------------------------");
		
		
	cmbAccounts.select(0);

		
		cmbAccounts.setLayoutData(grpData);

		
		lblCrDrAmount = new Label(grpEntry, SWT.NONE);
		lblCrDrAmount.setText("Amo&unt");
		grpData = new FormData();
		grpData.top = new FormAttachment(20);
		grpData.left = new FormAttachment(85);
		grpData.bottom = new FormAttachment(100);
		//grpData.right=new FormAttachment(10	);
		lblCrDrAmount.setLayoutData(grpData);
		
		txtCrDrAmount = new Text(grpEntry, SWT.BORDER | SWT.RIGHT);
		txtCrDrAmount.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		grpData= new FormData();
		grpData.top = new FormAttachment(10);
		grpData.left = new FormAttachment(lblCrDrAmount,8);
		grpData.bottom = new FormAttachment(90);
		grpData.right = new FormAttachment(98);
		//layout.bottom = new FormAttachment(12);
		txtCrDrAmount.setLayoutData(grpData);

		txtCrDrAmount.setText("0.00");

		grpEntry.pack();
		Label totalcr_dr=new Label (grpVoucher, SWT.BORDER);
		totalcr_dr.setText("Total Amount");
		grpData = new FormData();
		grpData.top = new FormAttachment(tblVoucherView.getTable());
		grpData.left = new FormAttachment(0);
		//pData.bottom = new FormAttachment(100);
		//grpData.right=new FormAttachment(100	);
		totalcr_dr.setLayoutData(grpData);
		
		totalDr=new Label (grpVoucher, SWT.BORDER | SWT.RIGHT);
		totalDr.setText("0.00");
		grpData = new FormData();
		grpData.top = new FormAttachment(tblVoucherView.getTable());
		grpData.left = new FormAttachment(58);
		//pData.bottom = new FormAttachment(100);
		grpData.right=new FormAttachment(74	);
		totalDr.setLayoutData(grpData);
		
		totalCr=new Label (grpVoucher, SWT.BORDER | SWT.RIGHT);
		totalCr.setText("0.00");
		grpData = new FormData();
		grpData.top = new FormAttachment(tblVoucherView.getTable());
		grpData.left = new FormAttachment(84);
		//pData.bottom = new FormAttachment(100);
		grpData.right=new FormAttachment(100	);
		totalCr.setLayoutData(grpData);
		
		this.makeaccssible(grpVoucher);
		this.makeaccssible(this);
		//this.pack();
		
		// this.setEvents();
		//this.setInitialVoucher();
		//this.setEvents();
		
		txtvoucherno.setFocus();
		btnsave.setEnabled(false);
		txtvoucherno.setSelection(0,txtvoucherno.getText().length());
		sdf.setLenient(false);
this.setEvents();
//this.setInitialVoucher();
Background =  new Color(this.getDisplay() ,220 , 224, 227);
Foreground = new Color(this.getDisplay() ,0, 0,0 );
FocusBackground  = new Color(this.getDisplay(),78,97,114 );
FocusForeground = new Color(this.getDisplay(),255,255,255);
BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);

globals.setThemeColor(this, Background, Foreground);
globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);

this.setInitialVoucher();
txtvoucherno.setBackground(FocusBackground);
txtvoucherno.setForeground(FocusForeground);

	}
	private void setInitialVoucher()
	{
		txtvoucherno.forceFocus();
		if(typeFlag.equals("Contra"))
		{
			cmbAccounts.setItems(transactionController.getContra());
			//cmbAccounts.add(cmbAccounts.add("----------------------------Please Select-------------------------------");
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);

		}
		if(typeFlag.equals("Payment"))
		{
			cmbAccounts.setItems(transactionController.getPayment("Dr"));
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);
	
		}
		if(typeFlag.equals("Journal"))
		{
			cmbAccounts.setItems(transactionController.getJournal());
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);

		}
		if(typeFlag.equals("Receipt"))
		{
			cmbAccounts.setItems(transactionController.getReceipt("Dr"));
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);

		}
		if(typeFlag.equals("Sales"))
		{
			cmbAccounts.setItems(transactionController.getSales("Dr"));
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);

		}
		if(typeFlag.equals("Purchase"))
		{
			cmbAccounts.setItems(transactionController.getPurchase("Dr"));
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);

		}
		if(typeFlag.equals("Credit Note"))
		{
			cmbAccounts.setItems(transactionController.getCreditNote("Dr"));
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);

		}
		if(typeFlag.equals("Debit Note"))
		{
			cmbAccounts.setItems(transactionController.getDebitNote("Dr"));
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);

		}
		if(typeFlag.equals("Sales Return"))
		{
			cmbAccounts.setItems(transactionController.getSales("Dr"));
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);

		}
		if(typeFlag.equals("Purchase Return"))
		{
			cmbAccounts.setItems(transactionController.getPurchase("Dr"));
			cmbAccounts.add("----------------------------Please Select-------------------------------",0);
			cmbAccounts.select(0);

		}

	}
	
	
	private void setEvents()

	{
		selectedIndex = -1;
		btnsave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				try {
					txtCrDrAmount.notifyListeners(SWT.TRAVERSE_RETURN , new Event());
					
					masterQueryParams.clear();
					detailQueryParams.clear();
					
					if(!txtddate.getText().equals("") && Integer.valueOf ( txtddate.getText())<10 && txtddate.getText().length()< txtddate.getTextLimit())
					{
						txtddate.setText("0"+ txtddate.getText());
					}
					if(!txtmdate.getText().equals("") && Integer.valueOf ( txtmdate.getText())<10 && txtmdate.getText().length()< txtmdate.getTextLimit())
					{
						txtmdate.setText("0"+ txtmdate.getText());
					}
					
					if(txtvoucherno.getText().trim().equals(""))
					{
						MessageBox msgVoucherCode = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
						msgVoucherCode.setText("Error!");
						msgVoucherCode.setMessage("Please Enter a Voucher Number");
						msgVoucherCode.open();
						txtvoucherno.setFocus();
						return;
					}
					if(txtddate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Date.");
						msgDayErr.open();
						txtddate.setFocus();
						
						return;
					}
					if(txtmdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setText("Month Validation Error!");
						msgDayErr.setMessage("Please enter a Month.");
						msgDayErr.open();
						txtmdate.setFocus();
						
						return;
					}
					
					
					if(txtyrdate.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msgDayErr.setText("Year Validation Error!");
						msgDayErr.setMessage("Please enter a Year.");
						msgDayErr.open();
						txtyrdate.setFocus();
						
						return;
					}
					if(!txtddate.getText().trim().equals("") && (Integer.valueOf(txtddate.getText())> 31 || Integer.valueOf(txtddate.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
						msgdateErr.setText("Date Validation Error!");
						msgdateErr.setMessage("You have entered an invalid Date");
						txtddate.setText("");
						txtddate.setFocus();
						msgdateErr.open();
						return;
					}
					if(!txtmdate.getText().trim().equals("") && (Integer.valueOf(txtmdate.getText())> 12 || Integer.valueOf(txtmdate.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
						msgdateErr.setText("Month Validation Error!");
						msgdateErr.setMessage("You have entered an invalid month");
						txtmdate.setText("");
						txtmdate.setFocus();
						msgdateErr.open();
						return;
					}
					
					if(!txtyrdate.getText().trim().equals("") && (Integer.valueOf(txtyrdate.getText())> 2100 || Integer.valueOf(txtyrdate.getText()) < 1900) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
						msgdateErr.setText("Year Validation Error!");
						msgdateErr.setMessage("You have entered an invalid Year");
						txtyrdate.setText("");
						txtyrdate.setFocus();
						msgdateErr.open();
						return;
					}
					
					
					
					try {
						Date startDate = sdf.parse(txtyrdate.getText() + "-" + txtmdate.getText() + "-" + txtddate.getText());
						
						} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Invalid Date");
						txtddate.setFocus();
						msg.open();
						return;
					}

					
					
					
					
					try {
						Date voucherDate = sdf.parse(txtyrdate.getText() + "-" + txtmdate.getText() + "-" + txtddate.getText());
						Date fromDate = sdf.parse(globals.session[2].toString().substring(6)+ "-" + globals.session[2].toString().substring(3,5) + "-"+ globals.session[2].toString().substring(0,2));
						Date toDate = sdf.parse(globals.session[3].toString().substring(6)+ "-" + globals.session[3].toString().substring(3,5) + "-"+ globals.session[3].toString().substring(0,2));
						
						if(voucherDate.compareTo(fromDate)< 0 || voucherDate.compareTo(toDate) > 0 )
						{
							MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK |SWT.ICON_WARNING );
							errMsg.setText("Warning!");
							errMsg.setMessage("The Voucher Date you entered is not within the Financial Year");
							errMsg.open();
							txtddate.setFocus();
							txtddate.setSelection(0,2);
							return;
						}
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.getMessage();
						MessageBox errMsg = new MessageBox(new Shell(),SWT.ERROR |SWT.OK |SWT.ICON_WARNING );
						errMsg.setText("Warning!");
						errMsg.setMessage("Improper Date Format");
						txtddate.setFocus();
						errMsg.open();
						return;
					}
					masterQueryParams.add(txtvoucherno.getText());
					masterQueryParams.add(sdf.format(new Date()));
					masterQueryParams.add(txtyrdate.getText()+"-" + txtmdate.getText()+ "-" + txtddate.getText());
					masterQueryParams.add(typeFlag);
					if(comboselprj.getItemCount() > 0 && comboselprj.getSelectionIndex() > 0)
					{
						masterQueryParams.add(comboselprj.getItem(comboselprj.getSelectionIndex()));
					}
					else
					{
						masterQueryParams.add("No Project");
					}
					masterQueryParams.add(txtnarration.getText());
					masterQueryParams.add("");
					//masterQueryParams.add(txtPurchaseyrdate.getText()+"-"+txtPurchasemdate.getText()+"-"+txtPurchaseddate.getText());
					//masterQueryParams.add(null); 
					masterQueryParams.add(txtyrdate.getText()+"-" + txtmdate.getText()+ "-" + txtddate.getText());
					masterQueryParams.add("0.00");
					for(int detailcounter = 0; detailcounter < tblVoucherView.getTable().getItemCount(); detailcounter ++ )
					{
						AddVoucher vRow = (AddVoucher) tblVoucherView.getElementAt(detailcounter);
						String[] DetailRow = new String[3];
						DetailRow[0] = vRow.getDrCr();
						DetailRow[1] = vRow.getAccountName();
						if(vRow.getDrCr().equals("Dr"))
						{
							DetailRow[2] =Double.toString(vRow.getDrAmount());
						}
						
					if(vRow.getDrCr().equals("Cr"))
					{
						DetailRow[2] =Double.toString(vRow.getCrAmount());
					}
					detailQueryParams.add(DetailRow);
					}
					MessageBox msgconfirm = new MessageBox(new Shell(),SWT.YES| SWT.NO|SWT.ICON_QUESTION);
					msgconfirm.setText("Voucher Confirmation for Voucher No. "+ txtvoucherno.getText() );
					msgconfirm.setMessage("Are you sure you want to save this Voucher?");
					int answer = msgconfirm.open();
					if(answer == SWT.YES)
					{
						if(transactionController.setTransaction(masterQueryParams, detailQueryParams) )
						{
							Composite grandParent = (Composite) btnsave.getParent().getParent().getParent().getParent();
							VoucherTabForm.typeFlag = VoucherTabForm.typeFlag;
							
							//lblsavemsg.setVisible(true);
							
							VoucherTabForm vtf = new VoucherTabForm(grandParent,SWT.NONE );
							
							AddNewVoucherComposite.lblsavemsg.setText("voucher saved with voucher No. " +txtvoucherno.getText());
							AddNewVoucherComposite.lblsavemsg.setVisible(true);
							btnsave.getParent().getParent().getParent().dispose();
							vtf.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
							
							}
										
						else
						{
							MessageBox success = new MessageBox(new Shell(),SWT.OK |SWT.ICON_ERROR);
							success.setText("Error!");
							success.setMessage("Error saving Voucher");
							success.open();
							

						}
							
					}
					else
					{
						txtvoucherno.setFocus();
						txtvoucherno.setSelection(0, 2);
					}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					MessageBox msg = new MessageBox(new Shell(),SWT.OK);
					
					if (txtddate.getText().contains(".")) 
					{
						msg.setMessage("Invalid Date");
						Display.getCurrent().asyncExec(new Runnable(){
							public void run()
							{
													
								txtddate.setText("");
							}

						});

							txtddate.setFocus();	
					}
					if (txtmdate.getText().contains(".")) 
					{
						msg.setMessage("Invalid Month");
						Display.getCurrent().asyncExec(new Runnable(){
							public void run()
							{
													
								txtmdate.setText("");
							}

						});
						txtmdate.setFocus();	
					}
					
					if (txtyrdate.getText().contains(".")) 
					{
						msg.setMessage("Invalid Year");
						Display.getCurrent().asyncExec(new Runnable(){
							public void run()
							{
													
								txtyrdate.setText("");
							}

						});
						txtyrdate.setFocus();	
					}
					msg.open();
				}


			}
		});
		tblVoucherView.getControl().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				tblVoucherView.getControl().setBackground(Background);
				tblVoucherView.getControl().setForeground(Foreground);
			};
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				tblVoucherView.getControl().setBackground(FocusBackground);
				tblVoucherView.getControl().setForeground(FocusForeground);

				focusflag = false;
			}
		});
		tblVoucherView.getControl().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode == SWT.DEL)
				{
					if(tblVoucherView.getTable().getSelectionIndex() >1 )
					{
						
					IStructuredSelection VoucherRow= (IStructuredSelection) tblVoucherView.getSelection();
					AddVoucher vr = (AddVoucher) VoucherRow.getFirstElement();
					tblVoucherView.remove(vr);
					totalDrAmount = 0;
					totalCrAmount = 0;
					for(int RowCounter = 0; RowCounter < tblVoucherView.getTable().getItemCount(); RowCounter ++ )
					{
						AddVoucher VoucherRowForTotal = (AddVoucher) tblVoucherView.getElementAt(RowCounter);
						totalDrAmount = totalDrAmount + VoucherRowForTotal.getDrAmount();
						totalCrAmount = totalCrAmount + VoucherRowForTotal.getCrAmount();
					}
					totalDr.setText(nf.format(totalDrAmount));
					totalCr.setText(nf.format(totalCrAmount));
					if(totalDrAmount!= totalCrAmount)
					{
						btnsave.setEnabled(false);
					}
					
					}
return;
				}
				if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
				{
				IStructuredSelection VoucherRow= (IStructuredSelection) tblVoucherView.getSelection();
				AddVoucher vr = (AddVoucher) VoucherRow.getFirstElement();
				try {
					if(vr.getAccountName().equals(""))
					{
						return;
					}
				} catch (NullPointerException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					cmbCr_Dr.removeAll();
					cmbCr_Dr.add("Dr");
					cmbCr_Dr.select(0);
					cmbCr_Dr.setEnabled(false);
					return;
				}
				
				
				grpEntry.setVisible(true);
				//lblCrDr.setVisible(true);
				cmbCr_Dr.setVisible(true);
				lblAccounts.setVisible(true);
				cmbAccounts.setVisible(true);
				lblCrDrAmount.setVisible(true);
				txtCrDrAmount.setVisible(true);
				tblVoucherView.getTable().setEnabled(false);
				cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
				try {
					selectedIndex = tblVoucherView.getTable().getSelectionIndex();
					alterFlag = true;
					//tblVoucherView.getTable().getItem(0).setChecked(true);
					if(vr.getDrCr().equals("Dr") )
					{
						totalDrAmount = totalDrAmount - vr.getDrAmount();
						totalDr.setText(nf.format(totalDrAmount));
						/*if (totalCr.getText().equals("Credit Amount")) 
						{
							totalCrAmount = 0.00;
							totalCr.setText(nf.format(totalCrAmount));
						}*/
						if (selectedIndex ==0) {
							cmbCr_Dr.removeAll();
							cmbCr_Dr.add("Dr");
							cmbCr_Dr.select(0);
							cmbCr_Dr.setEnabled(false);
						}
						else
						{
							cmbCr_Dr.setEnabled(true);
							cmbCr_Dr.select(0);
						}
					}
					if(vr.getDrCr().equals("Cr") )
					{
						totalCrAmount = totalCrAmount - vr.getCrAmount();
						totalCr.setText(nf.format(totalCrAmount));
						if (selectedIndex ==1) {
							cmbCr_Dr.removeAll();
							cmbCr_Dr.add("Cr");
							cmbCr_Dr.select(0);
							cmbCr_Dr.setEnabled(false);
						}
						else
						{
							cmbCr_Dr.setEnabled(true);
							cmbCr_Dr.select(1);
						}
						
					}

					
					tblVoucherView.remove(vr);
					tblVoucherView.insert(new AddVoucher("", "", 0.00, 0.00) ,selectedIndex );
					Display.getCurrent().asyncExec(new Runnable(){
						public void run()
						{
												
							cmbAccounts.setFocus();
						}

					});
					
					if(vr.getDrCr().equals("Dr"))
					{
						/*MessageBox msgdrcr = new MessageBox(new Shell(),SWT.OK);
						msgdrcr.setMessage("this row is of Dr type");
						msgdrcr.open();
						*/
						//cmbCr_Dr.select(0);
						txtCrDrAmount.setText(Double.toString(vr.getDrAmount()));
						if(typeFlag.equals("Contra"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
							
							
						}
						if(typeFlag.equals("Payment"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));

							
						}
						if(typeFlag.equals("Journal"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
							
							
						}
						if(typeFlag.equals("Receipt"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
							
							
						}
						if(typeFlag.equals("Sales"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

							
						}
						if(typeFlag.equals("Purchase"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
							
							
						}
						if(typeFlag.equals("Credit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));

							
						}
						if(typeFlag.equals("Debit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
							
							
						}
						if(typeFlag.equals("Sales Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

							
						}
						if(typeFlag.equals("Purchase Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
							

						}
						cmbAccounts.add(vr.getAccountName());
						cmbAccounts.select(cmbAccounts.getItemCount()-1);

					MessageBox msgacc = new MessageBox(new Shell(),SWT.OK);
					
					msgacc.setMessage(vr.getAccountName());
					//msgacc.open();
					for(int i = 0; i < cmbAccounts.getItemCount(); i ++ ){
						if(cmbAccounts.getItem(i).equals(vr.getAccountName()))
								{
							/*MessageBox msgmatch = new MessageBox(new Shell(),SWT.OK);
							msgmatch.setMessage(Integer.toString(i) + " var pakadla re! " + vr.getAccountName() );
							msgmatch.open();*/
							cmbAccounts.select(i);
								}
							
					}
					}
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					cmbCr_Dr.removeAll();
					cmbCr_Dr.add("Dr");
					cmbCr_Dr.select(0);
					return;
				}
				for(int i = 0; i < cmbAccounts.getItems().length; i++)
				{
					if(cmbAccounts.getItem(i).equals(vr.getAccountName()) )
					{
						cmbAccounts.select(i);
					}
				}
					
				if(vr.getDrCr().equals("Cr"))
				{
					//cmbCr_Dr.select(1);
					txtCrDrAmount.setText(Double.toString(vr.getCrAmount()));
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
						
						
					}
					if(typeFlag.equals("Payment"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));

						
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
						
						
					}
					if(typeFlag.equals("Receipt"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
						
						
					}
					if(typeFlag.equals("Sales"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

						
					}
					if(typeFlag.equals("Purchase"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
						
						
					}
					if(typeFlag.equals("Credit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

						
					}
					if(typeFlag.equals("Debit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
						
						
					}
					if(typeFlag.equals("Sales Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

						
					}
					if(typeFlag.equals("Purchase Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
						

					}
					cmbAccounts.add(vr.getAccountName());
					cmbAccounts.select(cmbAccounts.getItemCount()-1);
					
					for(int i = 0; i < cmbAccounts.getItemCount(); i ++ ){
					if(cmbAccounts.getItem(i).equals(vr.getAccountName()))
							{
						/*MessageBox msgmatch = new MessageBox(new Shell(),SWT.OK);
						msgmatch.setMessage(Integer.toString(i) + " var pakadla re! " + vr.getAccountName() );
						msgmatch.open();*/
						cmbAccounts.select(i);
							}
						
				}
				
				}
				for(int i = 0; i < cmbAccounts.getItems().length; i++)
				{
					if(cmbAccounts.getItem(i).equals(vr.getAccountName()) )
					{
						cmbAccounts.select(i);
					}
				}

				}
				

				
			}
		});
		tblVoucherView.getControl().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
		
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				//if(arg0.keyCode == SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
			{
		        IStructuredSelection VoucherRow= (IStructuredSelection) tblVoucherView.getSelection();
				AddVoucher vr = (AddVoucher) VoucherRow.getFirstElement();
				try {
					if(vr.getAccountName().equals(""))
					{
						return;
					}
				} catch (NullPointerException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					cmbCr_Dr.removeAll();
					cmbCr_Dr.add("Dr");
					cmbCr_Dr.select(0);
					cmbCr_Dr.setEnabled(false);
					return;
				}
				
				grpEntry.setVisible(true);
				//lblCrDr.setVisible(true);
				cmbCr_Dr.setVisible(true);
				lblAccounts.setVisible(true);
				cmbAccounts.setVisible(true);
				lblCrDrAmount.setVisible(true);
				txtCrDrAmount.setVisible(true);
				cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
				tblVoucherView.getTable().setEnabled(false);
				try {
					selectedIndex = tblVoucherView.getTable().getSelectionIndex();
					alterFlag = true;
					//tblVoucherView.getTable().getItem(0).setChecked(true);
					if(vr.getDrCr().equals("Dr") )
					{
						totalDrAmount = totalDrAmount - vr.getDrAmount();
						totalDr.setText(nf.format(totalDrAmount));
						if (selectedIndex ==0) {
							cmbCr_Dr.removeAll();
							cmbCr_Dr.add("Dr");
							cmbCr_Dr.select(0);
							cmbCr_Dr.setEnabled(false);
						}
						else
						{	cmbCr_Dr.setEnabled(true);
							cmbCr_Dr.select(0);
						}
					}
					if(vr.getDrCr().equals("Cr") )
					{
						totalCrAmount = totalCrAmount - vr.getCrAmount();
						totalCr.setText(nf.format(totalCrAmount));
						if (selectedIndex ==1) {
							cmbCr_Dr.removeAll();
							cmbCr_Dr.add("Cr");
							cmbCr_Dr.select(0);
							cmbCr_Dr.setEnabled(false);
						}
						else
						{
							cmbCr_Dr.setEnabled(true);
							cmbCr_Dr.select(1);
						}
					}

					
					tblVoucherView.remove(vr);
					tblVoucherView.insert(new AddVoucher("", "", 0.00, 0.00) ,selectedIndex );
					cmbAccounts.setFocus();
					if(vr.getDrCr().equals("Dr"))
					{
						/*MessageBox msgdrcr = new MessageBox(new Shell(),SWT.OK);
						msgdrcr.setMessage("this row is of Dr type");
						msgdrcr.open();
						*/
						cmbAccounts.setFocus();
						//cmbCr_Dr.select(0);
						txtCrDrAmount.setText(Double.toString(vr.getDrAmount()));
						if(typeFlag.equals("Contra"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
							
							
						}
						if(typeFlag.equals("Payment"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));

							
						}
						if(typeFlag.equals("Journal"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
							
							
						}
						if(typeFlag.equals("Receipt"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
							
							
						}
						if(typeFlag.equals("Sales"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

							
						}
						if(typeFlag.equals("Purchase"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
							
							
						}
						if(typeFlag.equals("Credit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));

							
						}
						if(typeFlag.equals("Debit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
							
							
						}
						if(typeFlag.equals("Sales Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

							
						}
						if(typeFlag.equals("Purchase Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
							

						}
						cmbAccounts.add(vr.getAccountName());
						cmbAccounts.select(cmbAccounts.getItemCount()-1);
						
						MessageBox msgacc = new MessageBox(new Shell(),SWT.OK);
					msgacc.setMessage(vr.getAccountName());
					//msgacc.open();
					for(int i = 0; i < cmbAccounts.getItemCount(); i ++ ){
						if(cmbAccounts.getItem(i).equals(vr.getAccountName()))
								{
							/*MessageBox msgmatch = new MessageBox(new Shell(),SWT.OK);
							msgmatch.setMessage(Integer.toString(i) + " var pakadla re! " + vr.getAccountName() );
							msgmatch.open();*/
							cmbAccounts.select(i);
							cmbAccounts.setFocus();
								}
							
					}
					}
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					cmbCr_Dr.removeAll();
					cmbCr_Dr.add("Dr");
					cmbCr_Dr.select(0);
					return;
				}
				for(int i = 0; i < cmbAccounts.getItems().length; i++)
				{
					if(cmbAccounts.getItem(i).equals(vr.getAccountName()) )
					{
						cmbAccounts.select(i);
					}
				}
					
				try {
					if(vr.getDrCr().equals("Cr"))
					{
						cmbAccounts.setFocus();
						//cmbCr_Dr.select(1);
						txtCrDrAmount.setText(Double.toString(vr.getCrAmount()));
						if(typeFlag.equals("Contra"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
							
							
						}
						if(typeFlag.equals("Payment"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));

							
						}
						if(typeFlag.equals("Journal"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
							
							
						}
						if(typeFlag.equals("Receipt"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
							
							
						}
						if(typeFlag.equals("Sales"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

							
						}
						if(typeFlag.equals("Purchase"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
							
							
						}
						if(typeFlag.equals("Credit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

							
						}
						if(typeFlag.equals("Debit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
							
							
						}
						if(typeFlag.equals("Sales Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

							
						}
						if(typeFlag.equals("Purchase Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
							

						}
						cmbAccounts.add(vr.getAccountName());
						cmbAccounts.select(cmbAccounts.getItemCount()-1);
						
						for(int i = 0; i < cmbAccounts.getItemCount(); i ++ ){
						if(cmbAccounts.getItem(i).equals(vr.getAccountName()))
								{
							/*MessageBox msgmatch = new MessageBox(new Shell(),SWT.OK);
							msgmatch.setMessage(Integer.toString(i) + " var pakadla re! " + vr.getAccountName() );
							msgmatch.open();*/
							cmbAccounts.select(i);
								}
							
					}
					
					}
					for(int i = 0; i < cmbAccounts.getItems().length; i++)
					{
						if(cmbAccounts.getItem(i).equals(vr.getAccountName()) )
						{
							cmbAccounts.select(i);
						}
					}
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					cmbCr_Dr.removeAll();
					cmbCr_Dr.add("Cr");
					cmbCr_Dr.select(0);
					return;
				}

				}
				

			
			}
		});

		
		cmbCr_Dr.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode ==  SWT.CR ||arg0.keyCode == SWT.KEYPAD_CR)
				{
					cmbAccounts.setFocus();
				}
			}
		});
		cmbCr_Dr.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Dr"))
						{
					if(typeFlag.equals("Contra"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Payment"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Journal"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Receipt"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Credit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Debit Note"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
						
						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Sales Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

						cmbAccounts.select(0);
					}
					if(typeFlag.equals("Purchase Return"))
					{
						cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
						
						cmbAccounts.select(0);

					}

					
						}
					if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Cr"))
							{
						if(typeFlag.equals("Contra"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Payment"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));

							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Journal"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Receipt"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Sales"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Purchase"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Credit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Debit Note"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
							
							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Sales Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

							cmbAccounts.select(0);
						}
						if(typeFlag.equals("Purchase Return"))
						{
							cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
							
							cmbAccounts.select(0);

						}

						
							}
			}
			});
		txtCrDrAmount.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	                return;
				}
				if(arg0.keyCode == SWT.KEYPAD_DECIMAL&& DecimalFlag == false)
				{
					DecimalFlag = true;
					return;
				}
				if(arg0.keyCode == SWT.KEYPAD_DECIMAL&& DecimalFlag == true)
				{
					arg0.doit= false;
					return;
				}
				
		if(arg0.keyCode == 46)
		{
			return;
		}
		if(arg0.keyCode == 45||arg0.keyCode == 62)
		{
			  arg0.doit = false;
			
		}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }
	        

				
			}
		});
		txtCrDrAmount.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.detail== SWT.TRAVERSE_TAB_PREVIOUS)
				{
					cmbAccounts.setFocus();
					return;
				}
				if(arg0.detail== SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit= false;
					return;
				}
				if(arg0.detail== SWT.TRAVERSE_TAB_NEXT)
				{
					if(comboselprj.getVisible())
					{
						comboselprj.setFocus();
						arg0.doit= false;
					}
					else
					{
						txtnarration.setFocus();
						arg0.doit= false;
					}
					return;
				}
								if(arg0.detail == SWT.TRAVERSE_RETURN)
				{
				//	focusflag = false;
									System.out.println("Traverse_return happened");
									

									if(txtCrDrAmount.getText().trim().equals("")|| txtCrDrAmount.getText().trim().startsWith("0") || txtCrDrAmount.getText().trim().startsWith(".") || txtCrDrAmount.getText().trim().endsWith(".")  )
									{
										MessageBox msgValueError = new MessageBox(new Shell(), SWT.OK | SWT.ICON_ERROR);
										msgValueError.setText("Error!");
										msgValueError.setMessage("Please Enter a proper Amount");
										msgValueError.open();
										arg0.doit = false;
										return;
											}
									
									//tblVoucherView.getTable().setEnabled(true);

									}
								if(arg0.keyCode == SWT.ARROW_UP)
								{
									cmbAccounts.setFocus();
									arg0.doit = false;
									return;
								}
								if(arg0.keyCode== SWT.ARROW_LEFT|| arg0.keyCode == SWT.ARROW_RIGHT)
								{
									arg0.doit = false;
									return;
								}
					arg0.doit = false;
					doTally();
					
				
				
			}
		});

		txtyrdate.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode== SWT.CR || arg0.keyCode == SWT.KEYPAD_CR)
				{
					txtyrdate.notifyListeners(SWT.FocusOut,new Event() );
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{	
					txtmdate.setFocus();
				}

				
			}
		});
		txtddate.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
					
				
				if(arg0.keyCode==46)
				{
					return;
				}
				if(arg0.keyCode == 45||arg0.keyCode == 62)
				{
					  arg0.doit = false;
					
				}
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	                return;
	        }
				if(arg0.keyCode  == 46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character))
	        {
	            arg0.doit = false;  // disallow the action
	        }

				

			}
		});
		
		txtmdate.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode==46)
				{
					return;
				}
				if(arg0.keyCode == 45||arg0.keyCode == 62)
				{
					  arg0.doit = false;
					
				}
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	                return;
	        }
				if(arg0.keyCode  == 46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character))
	        {
	            arg0.doit = false;  // disallow the action
	        }

				

			}
		});


		txtyrdate.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode==46)
				{
					return;
				}
				if(arg0.keyCode==45||arg0.keyCode == 62)
				{
					arg0.doit= false;
				}
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	                return;
	        }
				if(arg0.keyCode  == 46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character))
	        {
	            arg0.doit = false;  // disallow the action
	        }

				

			}
		});
		txtvoucherno.addFocusListener(new FocusAdapter() {
			/*@Override
			public void focusLost(FocusEvent arg0) {
				txtvoucherno.clearSelection();
				txtvoucherno.setBackground(Background);
				
				txtvoucherno.setForeground(Foreground);
			}*/
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
			/*	txtvoucherno.setBackground(FocusBackground);
				txtvoucherno.setForeground(FocusForeground);
*/
				
				if(totalCrAmount==totalDrAmount && totalCrAmount!=0.00 && totalDrAmount!=0.00 )
				{
					btnsave.setEnabled(true);
				}
				else if(totalCrAmount!=totalDrAmount || totalCrAmount==0.00 || totalDrAmount==0.00)
				{
					btnsave.setEnabled(false);
				}
				if(! lblsavemsg.getText().equals(""))
				{
					Display.getCurrent().asyncExec(new Runnable(){
						public void run()
						{
							long now = System.currentTimeMillis();
							long lblTimeOUt = 0;
							while(lblTimeOUt < (now + 2000))
							{
								lblTimeOUt = System.currentTimeMillis();
							}
		/*					MessageBox endtimer = new MessageBox(new Shell(),SWT.OK);
							endtimer.setText("time up! now will empty the label");
							endtimer.open();
		*/
							lblsavemsg.setText("");
							

						}
						
				});
/*					MessageBox msgtime = new MessageBox(new Shell(),SWT.OK);
					msgtime.setMessage("lable is not empty will run timmer");
					msgtime.open();
*/


				}
			}
			
		});
		txtvoucherno.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0); 65 to 90 caps , 97 122
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
						arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||
						arg0.keyCode == SWT.KEYPAD_CR||arg0.keyCode == SWT.ARROW_LEFT||arg0.keyCode == SWT.ARROW_RIGHT)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
				if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
				{
					txtddate.setFocus();
				}
			}
			});
			
			txtddate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
					{
						txtmdate.setFocus();
						
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{	
						txtvoucherno.setFocus();
					}
				}
				
			});
			
			txtmdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					
					if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
					{
						txtyrdate.setFocus();
						
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{	
						txtddate.setFocus();
					}
				}
			});
			
			
			
			comboselprj.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
				//	super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
					{
						txtnarration.setFocus();
						
					}
					if(arg0.keyCode == SWT.ARROW_UP && comboselprj.getSelectionIndex() == 0)
					{
						tblVoucherView.getTable().setFocus();
					}
					
					long now = System.currentTimeMillis();
					if (now > searchTexttimeout){
				         searchText = "";
				      }
					searchText += Character.toLowerCase(arg0.character);
					searchTexttimeout = now + 1000;					
					for(int i = 0; i < comboselprj.getItemCount(); i++ )
					{
						if(comboselprj.getItem(i).toLowerCase().startsWith(searchText ) ){
							//arg0.doit= false;
							comboselprj.select(i);
							comboselprj.notifyListeners(SWT.Selection ,new Event()  );
							break;
						}
					}
			
										
				}
			});
			
		
			txtnarration.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method{} stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.TAB||arg0.keyCode == SWT.KEYPAD_CR )
					{
						arg0.doit=false;
					}
					
					if(arg0.keyCode==SWT.CR|| arg0.keyCode==SWT.TAB||arg0.keyCode == SWT.KEYPAD_CR )
					{
						if(btnsave.isEnabled())
						{
							btnsave.setFocus();
							btnsave.notifyListeners(SWT.Selection, new Event());
						}
						
						
					}
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						if(comboselprj.isVisible())
						{
							comboselprj.setFocus();
						}
						else
						{
							tblVoucherView.getTable().setFocus();
						}
						
					}
				}
			}
			);
			txtnarration.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					if(!txtnarration.getText().trim().equals(""))
					{
						String orgname1 = txtnarration.getText();
						String titlecasename = toTitleCase(orgname1);
						txtnarration.setText(titlecasename);
					}
					
					
				}
			});
			
			
			btnsave.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
				//	super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_UP)
					{
						txtnarration.setFocus();
						
					}
					
				}		
			});
			
			txtmdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||
							arg0.keyCode == SWT.KEYPAD_CR||arg0.keyCode == SWT.ARROW_LEFT||arg0.keyCode == SWT.ARROW_RIGHT)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtyrdate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||
							arg0.keyCode == SWT.KEYPAD_CR||arg0.keyCode == SWT.ARROW_LEFT||arg0.keyCode == SWT.ARROW_RIGHT)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
				}
			});
			
			txtddate.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||
							arg0.keyCode == SWT.KEYPAD_CR||arg0.keyCode == SWT.ARROW_LEFT||arg0.keyCode == SWT.ARROW_RIGHT)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
					
				}
			});
			
			txtddate.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0)
				{
					//verifyFlag = true;
					/*txtddate.clearSelection();
					txtddate.setBackground(FocusBackground);
					txtddate.setForeground(FocusForeground);
					*/if(totalCrAmount==totalDrAmount && totalCrAmount!=0.00 && totalDrAmount!=0.00 )
					{
						btnsave.setVisible(true);
					}
					
				}
				
				
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					//verifyFlag = false;
					/*txtddate.setBackground(Background);
					txtddate.setForeground(Foreground);
					*/try {
						
						if(!txtddate.getText().equals("") && Integer.valueOf ( txtddate.getText())<10 && txtddate.getText().length()< txtddate.getTextLimit())
						{
							txtddate.setText("0"+ txtddate.getText());
							//txtFromDtMonth.setFocus();
							return;
							
							
							
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/*if(txtFromDtDay.getText().length()==2)
					   {
						   txtFromDtMonth.setFocus();
					   }*/
				}
			});
			
			txtmdate.addFocusListener(new FocusAdapter() {
				public void focusLost(FocusEvent arg0) {
					try {
						
						if(!txtmdate.getText().equals("") && Integer.valueOf ( txtmdate.getText())<10 && txtmdate.getText().length()< txtmdate.getTextLimit())
						{
							txtmdate.setText("0"+ txtmdate.getText());
							//txtFromDtMonth.setFocus();
							return;
							
							
							
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				};
			});
			
			txtyrdate.addFocusListener(new FocusAdapter() {
			/*	@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					txtyrdate.clearSelection();
					txtyrdate.setBackground(FocusBackground);
					txtyrdate.setForeground(FocusForeground);
				}
			*/	@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
			/*		txtyrdate.setBackground(Background);
					txtyrdate.setForeground(Foreground);
			*/
				if(tblVoucherView.getTable().getItemCount() == 0)
				{
					cmbAccounts.setFocus();
				}
				else
				{
					tblVoucherView.getTable().setFocus();
					
				}


				}
			});
			cmbAccounts.addTraverseListener(new TraverseListener() {
				
				@Override
				public void keyTraversed(TraverseEvent arg0) {
					// TODO Auto-generated method stub
					if(arg0.detail==SWT.TRAVERSE_MNEMONIC)
					{
						cmbAccounts.setListVisible(false);
						txtCrDrAmount.setFocus();
						arg0.doit = true;
					}
					if (arg0.detail==SWT.TRAVERSE_TAB_NEXT || arg0.detail==SWT.TRAVERSE_TAB_PREVIOUS) {
						cmbAccounts.setListVisible(false);	
					}
				}
			});;
			
			cmbAccounts.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					cmbAccounts.setListVisible(true);
				crdrcombofocus = cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex());
						cmbAccounts.setBackground(FocusBackground);
						cmbAccounts.setForeground(FocusForeground);
				
				
					
					focusflag = true;
					//cmbAccounts.setListVisible(true);
					if(cmbAccounts.getSelectionIndex()>= 1  )
					{
						if(SelectedAccounts.contains(cmbAccounts.getItem(cmbAccounts.getSelectionIndex())))
						{
							SelectedAccounts.remove(cmbAccounts.getItem(cmbAccounts.getSelectionIndex()));
						}
					}
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					//cmbAccounts.clearSelection();
					//txtCrDrAmount.setText("");
					cmbAccounts.setListVisible(false);
					focusflag = false;
					
                    	cmbAccounts.setBackground(Background );
						cmbAccounts.setForeground(Foreground);
					
				}
				
				
						
			});
			cmbCr_Dr.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					crdrcombofocus = cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex());
					cmbCr_Dr.setBackground(FocusBackground);
					cmbCr_Dr.setForeground(FocusForeground);	
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					cmbCr_Dr.setBackground( Background);
					cmbCr_Dr.setForeground( Foreground);
				}
			});
			
			txtCrDrAmount.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					
					txtCrDrAmount.clearSelection();
					txtCrDrAmount.setBackground(FocusBackground);
					txtCrDrAmount.setForeground(FocusForeground);
					txtCrDrAmount.setSelection(0, txtCrDrAmount.getText().length() );
					
				}
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					txtCrDrAmount.setBackground( Background);
					txtCrDrAmount.setForeground( Foreground);
				}

			});
			cmbAccounts.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(((arg0.stateMask & SWT.ALT) == SWT.ALT) && arg0.character == 'o')
					{
						cmbAccounts.setListVisible(false);
						btnAddAccount.notifyListeners(SWT.Selection, new Event());
						return;
					}
					
				
					if(arg0.keyCode== SWT.CR|| arg0.keyCode == SWT.KEYPAD_CR )
					{
						cmbAccounts.setListVisible(false);
						txtCrDrAmount.setFocus();
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						if(cmbAccounts.getSelectionIndex() == 0)
						{
							txtyrdate.setFocus();
						}
					}
					long now = System.currentTimeMillis();
					if (now > searchTexttimeout){
				         searchText = "";
				      }
					searchText += Character.toLowerCase(arg0.character);
					searchTexttimeout = now + 500;					
					for(int i = 0; i < cmbAccounts.getItemCount(); i++ )
					{				
						if(cmbAccounts.getItem(i).toLowerCase().startsWith(searchText ) ){
							//arg0.doit= false;
							cmbAccounts.select(i);
							//cmbAccounts.notifyListeners(SWT.Selection ,new Event()  );
							break;
						}
						
				}
				
				}
				
			});
			txtCrDrAmount.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						focusflag = true;
					}
					if(arg0.keyCode== SWT.CR || arg0.keyCode== SWT.KEYPAD_CR )
					{
						if(txtCrDrAmount.getText().trim().equals("0.00")|| txtCrDrAmount.getText().trim().equals("")|| txtCrDrAmount.getText().trim().startsWith("0"))
						{
							focusflag = true;
							MessageBox msgerr = new MessageBox(new Shell(),SWT.OK| SWT.ERROR |SWT.ICON_ERROR );
							msgerr.setText("Improper or No amount entered");
							msgerr.setMessage("Please Enter a proper amount");
							//msgerr.open();
							txtCrDrAmount.setText("0.00");
							

							return;
						}
						else
						{
							focusflag = false;
						}
						
						txtCrDrAmount.notifyListeners(SWT.FocusOut, new Event());
						return;
					}
					if(arg0.keyCode == SWT.ARROW_UP)
					{
						cmbAccounts.setFocus();
					}
				}
			});
	
			btnAddAccount.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					
					//super.widgetSelected(arg0);
					Shell shell = new Shell();
					AddAccountPopup  dialog = new AddAccountPopup(shell);
					//cmbAccounts.select(0);
					
					dialog.open();
					if(AddAccountPopup.cancelflag.equals(true))
					{
						shell.dispose();
						cmbAccounts.setFocus();
						//return;
					}
					else
					{
						int curindex = cmbAccounts.getSelectionIndex();
						/*MessageBox msg = new MessageBox(new Shell(), SWT.OK);
						msg.setMessage(crdrcombofocus);*/
						if(typeFlag.equals("Contra"))
						{
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
									
									cmbAccounts.setFocus();
								}
							});
							
						}
						if(typeFlag.equals("Journal"))
						{
							
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									int curindex = cmbAccounts.getSelectionIndex();
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
									
									cmbAccounts.setFocus();
								}
							});
							
							
						}
						if (typeFlag.equals("Payment")) 
						{
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									int curindex = cmbAccounts.getSelectionIndex();
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment(crdrcombofocus)));
									
									cmbAccounts.setFocus();
								}
							});
							
							
						}
						if (typeFlag.equals("Receipt")) 
						{
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									int curindex = cmbAccounts.getSelectionIndex();
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt(crdrcombofocus)));
									
									cmbAccounts.setFocus();
								}
							});
							
							
						}
						if (typeFlag.equals("Sales")) 
						{
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									int curindex = cmbAccounts.getSelectionIndex();
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales(crdrcombofocus)));
									
									cmbAccounts.setFocus();
								}
							});
							
						}
						if (typeFlag.equals("Purchase")) 
						{
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									int curindex = cmbAccounts.getSelectionIndex();
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase(crdrcombofocus)));
									
									cmbAccounts.setFocus();
								}
							});
							
							
						}
						if (typeFlag.equals("Credit Note")) 
						{
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									int curindex = cmbAccounts.getSelectionIndex();
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote(crdrcombofocus)));
									
									cmbAccounts.setFocus();
								}
							});
							
						}
						if (typeFlag.equals("Debit Note")) 
						{
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									int curindex = cmbAccounts.getSelectionIndex();
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote(crdrcombofocus)));
									
									cmbAccounts.setFocus();
								}
							});
							
						}
						if (typeFlag.equals("Sales Return")) 
						{
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									int curindex = cmbAccounts.getSelectionIndex();
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getSalesReturn(crdrcombofocus)));
									
									cmbAccounts.setFocus();
								}
							});
							
						}
						if (typeFlag.equals("Purchase Return")) 
						{
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									int curindex = cmbAccounts.getSelectionIndex();
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchaseReturn(crdrcombofocus)));

									
									cmbAccounts.setFocus();
								}
							});
							
						}
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								for(int i = 0; i < cmbAccounts.getItemCount(); i++ )
								{
									if(cmbAccounts.getItem(i).equalsIgnoreCase(AddAccountPopup.newAccount) ){
										//arg0.doit= false;
										cmbAccounts.select(i);
										cmbAccounts.notifyListeners(SWT.Selection ,new Event()  );
										break;
									}
								}							
								}
						});
						
					}

				}
			});
}
private String[] getFilteredAccountList(String[] OrigList) {
	ArrayList<String> filterAccounts = new ArrayList<String>();
	
	for(int filtercounter = 0; filtercounter < OrigList.length; filtercounter++ )
	{
		if(!SelectedAccounts.contains(OrigList[filtercounter]))
		{
			filterAccounts.add(OrigList[filtercounter]);
			
		}
	}
	for (int i = 0; i < tblVoucherView.getTable().getItemCount(); i++) 
	{
		TableItem tbitm= tblVoucherView.getTable().getItem(i);
		System.out.println("acc name column: "+tbitm.getText(0));
		filterAccounts.remove(tbitm.getText(0));
	}

	filterAccounts.add(0,"----------------------------Please select----------------------------");
	String[] FinalList = new String[filterAccounts.size() ];
	for(int convert = 0; convert < filterAccounts.size(); convert ++)
	{
		FinalList[convert] = filterAccounts.get(convert); 
	}
	return FinalList;
}
private void doTally()
{
	// from here
	// TODO Auto-generated method stub
	//super.focusLost(arg0);
	try {
		String testamount = nf.format(Double.parseDouble(txtCrDrAmount.getText().trim() ));
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
		MessageBox msgerr = new MessageBox(new Shell(),SWT.OK| SWT.ERROR |SWT.ICON_ERROR);
		msgerr.setText("Invalid Amount");
		msgerr.setMessage("You have entered an Invalid Amount");
		msgerr.open();
		tblVoucherView.getTable().setEnabled(false);
		return;
	}

	if(cmbAccounts.getSelectionIndex()== -1 || cmbAccounts.getSelectionIndex()== 0)
	{
		MessageBox msgvalidate = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
		msgvalidate.setText("Error!");
		msgvalidate.setMessage("Please Select an Account");
		//msgvalidate.open();
		cmbAccounts.setFocus();
			return;
		
	}

//	if(cmbAccounts. )
	if(txtCrDrAmount.getText().trim().equals("0.00") || txtCrDrAmount.getText().trim().equals(""))
	{
		MessageBox msgamount = new MessageBox(new Shell(), SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
		msgamount.setText("Error!");
		msgamount.setMessage("Please Enter the Amount");
		//msgamount.open();
		Display.getCurrent().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				txtCrDrAmount.setFocus();
				txtCrDrAmount.setText("");
			}
		});
		
		
		return;
	}
	
	if(cmbAccounts.getSelectionIndex()>= 1  )
	{
		SelectedAccounts.add(cmbAccounts.getItem(cmbAccounts.getSelectionIndex()));
	}
	
	if(tblVoucherView.getTable().getItemCount()<= 1)
	{
		if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Dr"))
			{
			/*MessageBox msgdr = new MessageBox(new Shell(),SWT.OK);
			msgdr.setMessage("dr is selected");
			msgdr.open();
			*/
			AddVoucher row = new AddVoucher("Dr", cmbAccounts.getItem(cmbAccounts.getSelectionIndex() ) , Double.parseDouble(txtCrDrAmount.getText() ),0 );
			totalDr.setText(txtCrDrAmount.getText() );
			cmbCr_Dr.removeAll();
			cmbCr_Dr.add("Cr");
			cmbCr_Dr.select(0);
			cmbCr_Dr.setEnabled(false);
			//txtCrDrAmount.setText();
			if(typeFlag.equals("Contra"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
				
				cmbAccounts.select(0);
			}
			if(typeFlag.equals("Payment"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));

				cmbAccounts.select(0);
			}
			if(typeFlag.equals("Journal"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
				
				cmbAccounts.select(0);
			}
			if(typeFlag.equals("Receipt"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
				
				cmbAccounts.select(0);
			}
			if(typeFlag.equals("Sales"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

				cmbAccounts.select(0);
			}
			if(typeFlag.equals("Purchase"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
				
				cmbAccounts.select(0);
			}
			if(typeFlag.equals("Credit Note"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

				cmbAccounts.select(0);
			}
			if(typeFlag.equals("Debit Note"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
				
				cmbAccounts.select(0);
			}
			if(typeFlag.equals("Sales Return"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

				cmbAccounts.select(0);
			}
			if(typeFlag.equals("Purchase Return"))
			{
				cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
				
				cmbAccounts.select(0);

			}
//condition to either add new or edit existing row.
			cmbAccounts.setFocus();
			if(alterFlag == false)
			{
				tblVoucherView.add(row);
				tblVoucherView.getTable().setEnabled(true);
			}
			else
			{
				MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
				AddVoucher removableRow = (AddVoucher) tblVoucherView.getElementAt(selectedIndex);
				msgtest.setMessage("This is the first row and it is Dr and account name is "+ removableRow.getAccountName()  );
				//msgtest.open();
				tblVoucherView.remove(removableRow);
				
				tblVoucherView.insert(row, selectedIndex);
				tblVoucherView.getTable().setEnabled(true);
				alterFlag = false;
			}
			


			alterFlag = false;
			return;

			}

						if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Cr"))
	{
							AddVoucher row = new AddVoucher("Cr",cmbAccounts.getItem(cmbAccounts.getSelectionIndex()) , 0, Double.parseDouble(txtCrDrAmount.getText()));
							if(alterFlag == false)
							{
								MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
								msgtest.setMessage("alter flag is false and we are in Cr second row");
								//msgtest.open();

								tblVoucherView.add(row);
								tblVoucherView.getTable().setEnabled(true);
								cmbCr_Dr.setEnabled(true);
							}
							else
							{
								MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
								AddVoucher removeableRow = (AddVoucher) tblVoucherView.getElementAt(selectedIndex);
								tblVoucherView.remove(removeableRow);
								msgtest.setMessage("alter flag is true and we are in Cr second row .  The account name is " + removeableRow.getAccountName() );
								//msgtest.open();
								
								tblVoucherView.insert(row, selectedIndex);
								tblVoucherView.getTable().setEnabled(true);
								alterFlag = false;
							}
							

							totalCr.setText(txtCrDrAmount.getText());
							totalDrAmount = 0;
							totalCrAmount = 0;
							for(int RowCounter = 0; RowCounter < tblVoucherView.getTable().getItemCount(); RowCounter ++ )
							{
								AddVoucher VoucherRow = (AddVoucher) tblVoucherView.getElementAt(RowCounter);
								totalDrAmount = totalDrAmount + VoucherRow.getDrAmount();
								totalCrAmount = totalCrAmount + VoucherRow.getCrAmount();
							}
							totalDr.setText(nf.format(totalDrAmount));
							totalCr.setText(nf.format(totalCrAmount));
							if(totalDrAmount == totalCrAmount)
							{
								
								cmbCr_Dr.removeAll();
								cmbAccounts.removeAll();
								txtCrDrAmount.setText("");
								//lblCrDr.setVisible(false);
								cmbCr_Dr.setVisible(false);
								lblAccounts.setVisible(false);
								cmbAccounts.setVisible(false);
								tblVoucherView.getTable().setEnabled(true);
								lblCrDrAmount.setVisible(false);
								txtCrDrAmount.setVisible(false);
								grpEntry.setVisible(false);
								btnsave.setEnabled(true);
								if(comboselprj.getVisible())
								{
									comboselprj.setFocus();
								}
								else
								{
									txtnarration.setFocus();
								}
								return;
							}
							if(totalDrAmount> totalCrAmount)
							{
								cmbCr_Dr.removeAll();
								cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
								cmbCr_Dr.select(1);
								if(typeFlag.equals("Contra"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Payment"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Journal"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Receipt"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Sales"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Purchase"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Credit Note"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Debit Note"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Sales Return"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Purchase Return"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
									
									cmbAccounts.select(0);

								}


								cmbCr_Dr.setFocus();
								txtCrDrAmount.setText(Double.toString(totalDrAmount - totalCrAmount));

							}
							if(totalCrAmount> totalDrAmount)
							{
								cmbCr_Dr.removeAll();
								cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
								cmbCr_Dr.select(0);
								if(typeFlag.equals("Contra"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Payment"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Journal"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Receipt"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Sales"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Purchase"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Credit Note"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Debit Note"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Sales Return"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));
									
									cmbAccounts.select(0);
								}
								if(typeFlag.equals("Purchase Return"))
								{
									cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
									
									cmbAccounts.select(0);

								}


								cmbCr_Dr.setFocus();
								txtCrDrAmount.setText(Double.toString(totalCrAmount - totalDrAmount));

							}
							

	}
						alterFlag = false;
						return;

	}
	if(tblVoucherView.getTable().getItemCount() > 1 )
	{
		//todo
		//cmbCr_Dr.setEnabled(true);
		if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Dr"))
		{
			AddVoucher row = new AddVoucher("Dr", cmbAccounts.getItem(cmbAccounts.getSelectionIndex() ) , Double.parseDouble(txtCrDrAmount.getText() ),0 );
			if(alterFlag == false)
			{
				tblVoucherView.add(row);
				tblVoucherView.getTable().setEnabled(true);
			}
			else
			{

				//MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
				AddVoucher removableRow = (AddVoucher) tblVoucherView.getElementAt(selectedIndex);
				//msgtest.setMessage("This is the first row and it is Dr and account name is "+ removableRow.getAccountName()  );
				//msgtest.open();
				tblVoucherView.remove(removableRow);
				
				tblVoucherView.insert(row, selectedIndex);
				tblVoucherView.getTable().setEnabled(true);
				alterFlag = false;
			}
			//process for tally of transaction follows.
			totalDrAmount = 0;
			totalCrAmount = 0;
			for(int RowCounter = 0; RowCounter < tblVoucherView.getTable().getItemCount(); RowCounter ++ )
			{
				AddVoucher VoucherRow = (AddVoucher) tblVoucherView.getElementAt(RowCounter);
				totalDrAmount = totalDrAmount + VoucherRow.getDrAmount();
				totalCrAmount = totalCrAmount + VoucherRow.getCrAmount();
			}
			totalDr.setText(nf.format(totalDrAmount));
			totalCr.setText(nf.format(totalCrAmount));
			if(totalDrAmount == totalCrAmount)
			{
				cmbCr_Dr.removeAll();
				cmbAccounts.removeAll();
				txtCrDrAmount.setText("");
				//lblCrDr.setVisible(false);
				cmbCr_Dr.setVisible(false);
				lblAccounts.setVisible(false);
				cmbAccounts.setVisible(false);
				tblVoucherView.getTable().setEnabled(true);
				lblCrDrAmount.setVisible(false);
				txtCrDrAmount.setVisible(false);
				txtnarration.setFocus();
				return;
			}
			
			if(selectedIndex < (tblVoucherView.getTable().getItemCount() -1)&& selectedIndex!= -1 )
			{
				//don't add an extra row.
				cmbCr_Dr.removeAll();
				cmbAccounts.removeAll();
				txtCrDrAmount.setText("");
				//lblCrDr.setVisible(false);
				cmbCr_Dr.setVisible(false);
				lblAccounts.setVisible(false);
				cmbAccounts.setVisible(false);
				tblVoucherView.getTable().setEnabled(true);
				lblCrDrAmount.setVisible(false);
				txtCrDrAmount.setVisible(false);
				tblVoucherView.getTable().setFocus();
				tblVoucherView.getTable().select(selectedIndex+1);
				return;

			}
			if(totalDrAmount> totalCrAmount)
			{
				cmbCr_Dr.removeAll();
				cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
				cmbCr_Dr.select(1);
				MessageBox msgdr = new MessageBox(new Shell(),SWT.OK);
				msgdr.setMessage("we are in the condition where Dr is more so a Cr row will come for editing");
				//msgdr.open();
				if(typeFlag.equals("Contra"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Payment"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Journal"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Receipt"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Credit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Debit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));
					
					cmbAccounts.select(0);

				}


				cmbCr_Dr.setFocus();
				MessageBox msgfocus = new MessageBox(new Shell(),SWT.OK);
				msgfocus.setMessage("focused on CRDR now we can take actions.");
				//msgfocus.open();
				txtCrDrAmount.setText(Double.toString(totalDrAmount - totalCrAmount));
				return;

			}
			if(totalCrAmount> totalDrAmount)
			{
				cmbCr_Dr.removeAll();
				cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
				cmbCr_Dr.select(0);
				if(typeFlag.equals("Contra"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
				
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Payment"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));
				
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Journal"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Receipt"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Credit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Debit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));
					
					cmbAccounts.select(0);

				}


				cmbCr_Dr.setFocus();
				txtCrDrAmount.setText(Double.toString(totalCrAmount - totalDrAmount));

			}


		}
		MessageBox msgindex = new MessageBox(new Shell(),SWT.OK);
		msgindex.setMessage("the selected index is "+ Integer.toString(cmbCr_Dr.getSelectionIndex()));
	//	msgindex.open();
		if(cmbCr_Dr.getItem(cmbCr_Dr.getSelectionIndex()).equals("Cr"))
		{
			AddVoucher row = new AddVoucher("Cr", cmbAccounts.getItem(cmbAccounts.getSelectionIndex() ) ,0.00, Double.parseDouble(txtCrDrAmount.getText() ));
			if(alterFlag == false)
			{

				tblVoucherView.add(row);
				tblVoucherView.getTable().setEnabled(true);
			}
			else
			{

				MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
				AddVoucher removableRow = (AddVoucher) tblVoucherView.getElementAt(selectedIndex);
				msgtest.setMessage("This is the first row and it is Dr and account name is "+ removableRow.getAccountName()  );
				//msgtest.open();
				tblVoucherView.remove(removableRow);
				
				tblVoucherView.insert(row, selectedIndex);
				tblVoucherView.getTable().setEnabled(true);
				alterFlag = false;
			}

			//process for tally of transaction follows.
			totalDrAmount = 0;
			totalCrAmount = 0;
			for(int RowCounter = 0; RowCounter < tblVoucherView.getTable().getItemCount(); RowCounter ++ )
			{
				AddVoucher VoucherRow = (AddVoucher) tblVoucherView.getElementAt(RowCounter);
				totalDrAmount = totalDrAmount + VoucherRow.getDrAmount();
				totalCrAmount = totalCrAmount + VoucherRow.getCrAmount();
			}
			totalDr.setText(nf.format(totalDrAmount));
			totalCr.setText(nf.format(totalCrAmount));
			if(totalDrAmount == totalCrAmount)
			{
				cmbCr_Dr.removeAll();
				cmbAccounts.removeAll();
				txtCrDrAmount.setText("");
				//lblCrDr.setVisible(false);
				cmbCr_Dr.setVisible(false);
				lblAccounts.setVisible(false);
				cmbAccounts.setVisible(false);
				tblVoucherView.getTable().setEnabled(true);
				lblCrDrAmount.setVisible(false);
				txtCrDrAmount.setVisible(false);
				grpEntry.setVisible(false);
				btnsave.setEnabled(true);
				txtnarration.setFocus();
				return;
			}
			
			if(selectedIndex < (tblVoucherView.getTable().getItemCount() -1)&& selectedIndex!= -1  )
			{
				//don't add an extra row.
				cmbCr_Dr.removeAll();
				cmbAccounts.removeAll();
				txtCrDrAmount.setText("");
				//lblCrDr.setVisible(false);
				cmbCr_Dr.setVisible(false);
				lblAccounts.setVisible(false);
				cmbAccounts.setVisible(false);
				tblVoucherView.getTable().setEnabled(true);
				lblCrDrAmount.setVisible(false);
				txtCrDrAmount.setVisible(false);
				tblVoucherView.getTable().setFocus();
				tblVoucherView.getTable().select(selectedIndex+1);
				return;

			}

			if(totalDrAmount> totalCrAmount)
			{
				cmbCr_Dr.removeAll();
				cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
				cmbCr_Dr.select(1);
				if(typeFlag.equals("Contra"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Payment"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Cr")));
					
					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Journal"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Receipt"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Credit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Debit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Cr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Cr")));

					cmbAccounts.select(0);

				}


				cmbCr_Dr.setFocus();
				txtCrDrAmount.setText(Double.toString(totalDrAmount - totalCrAmount));

			}
			if(totalCrAmount> totalDrAmount)
			{
				cmbCr_Dr.removeAll();
				cmbCr_Dr.setItems(new String[]{"Dr","Cr"} );
				cmbCr_Dr.select(0);
				if(typeFlag.equals("Contra"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getContra()));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Payment"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPayment("Dr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Journal"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getJournal()));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Receipt"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getReceipt("Dr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Credit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getCreditNote("Dr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Debit Note"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getDebitNote("Dr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Sales Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getSales("Dr")));

					cmbAccounts.select(0);
				}
				if(typeFlag.equals("Purchase Return"))
				{
					cmbAccounts.setItems(getFilteredAccountList(transactionController.getPurchase("Dr")));

					cmbAccounts.select(0);

				}


				cmbCr_Dr.setFocus();
				txtCrDrAmount.setText(Double.toString(totalCrAmount - totalDrAmount));

		}

	}
	
	
	
	
	}
if(tblVoucherView.getTable().getItemCount()>9 )
{
//tblVoucherView.scrollDown(0, 50);
tblVoucherView.getTable().setTopIndex(tblVoucherView.getTable().getItemCount()-1 );

}
if(totalCrAmount != totalDrAmount|| totalCrAmount == 0 || totalDrAmount == 0)
{
	btnsave.setEnabled(false); 
}
alterFlag = false;
//till here.

}
public static String toTitleCase(String input) 
{
    StringBuilder titleCase = new StringBuilder();
    boolean nextTitleCase = true;
    char second_char=0;
    String[] seperated_by_space =input.split(" "); 
    try {
		for(int i= 0; i<seperated_by_space.length; i++)
		{
			if (seperated_by_space[i].length() > 1)
				{
					second_char = seperated_by_space[i].charAt(1);
				} 
				else if (seperated_by_space[i].length() == 1)
				{
					second_char = seperated_by_space[i].charAt(0);
				}
				if (Character.isDigit(second_char))
				{
					for (char c : seperated_by_space[i].toCharArray())
					{

						titleCase.append(c);
					}	
				}
				if (!Character.isAlphabetic(second_char) &&!Character.isDigit(second_char) )
				{
					for (char c : seperated_by_space[i].toCharArray())
					{

						titleCase.append(c);
					}
				}
				if (Character.isLowerCase(second_char))
				{
					for (char c : seperated_by_space[i].toCharArray())
					{
						if (nextTitleCase) {
							c = Character.toTitleCase(c);
							nextTitleCase = false;
						}

						titleCase.append(c);
					}
				} 
				else if (Character.isUpperCase(second_char)) 
				{
					for (char c : seperated_by_space[i].toCharArray())
					{

						titleCase.append(c);
					}
				}
				
			
			
			if (i != seperated_by_space.length - 1) 
			{
				titleCase.append(" ");
				nextTitleCase = false;
			}
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    return titleCase.toString();
}	


			public void makeaccssible(Control c) {
				c.getAccessible();
			}

		}


