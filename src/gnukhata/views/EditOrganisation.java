package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.StartupController;
import gnukhata.controllers.transactionController;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.omg.CORBA.PUBLIC_MEMBER;

public class EditOrganisation extends Composite 
{
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	public static Label lblsavemsg;
	static Display display;
	
	Label lblLogo;
	Label lblOrgDetails;
	Label lblLine;
	Label lblheadline;
	Label lbleditorg;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strOrgType;
	Label lblRegiNo;
	Text txtRegiNo;
	Label lblFcraRegiNo;
	Text txtFcraRegiNo;
	Label lblAddress;
	Text txtAddress;
	Label lblCountry;
	long searchTexttimeout = 0;
	String state;
	String searchText = "";
	CCombo dropdownCountry;
	Label lblState;
	CCombo dropdownState;
	Label lblCity;
	CCombo dropdownCity;
	Label lblPostalCode;
	Text txtPostalCode;
	Label lblvatNo;
	Text txtvatNo ;
	Label lblDtOfRegiNo;
	Label lblDtOfRegiNoDash1;
	Label lblDtOfRegiNoDash2;
	Text txtDtDOrg;
	Text txtDtMOrg;
	Text txtDtYOrg;
	Label lblDtFormat;
	Label lblDtOfFcraRegiNo;
	Label lblDtOfFcraRegiNoDash1;
	Label lblDtOfFcraRegiNoDash2;
	Label lblSkip;
	Label lblBack;
	Label lblSave;
	
	Text txtDtDOfFcraRegiNo;
	Text txtDtMOfFcraRegiNo;
	Text txtDtYOfFcraRegiNo;
	Label lblEmailId;
	Text txtEmailId ;
	Label lblTeliphoneNo;
	Text txtTeliphoneNo;  
	Label lblFaxNo;
	Text txtFaxNo;
	Label lblWebsite;
	Text txtWebsite;
	Label lblPAN ;
	Text txtPAN;
	Label lblServiceTaxNo ;
	Text txtServiceTaxNo;
	Button btnSave;
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	Vector<Object> params;

	public EditOrganisation(Composite parent, int style) 
	{
		super(parent, style);
		// TODO Auto-generated constructor stub
		sdf.setLenient(false);
		strOrgName = globals.session[1].toString();
		strFromYear= globals.session[2].toString();
		strToYear  = globals.session[3].toString();
		
		MainShell.lblLogo.setVisible(false);
		MainShell.lblLine.setVisible(false);
		MainShell.lblOrgDetails.setVisible(false);
		
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);	
		FormData layout =new FormData();
		
		lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(60);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);
		
		lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(14);
		lblLine.setLayoutData(layout);
		
		lbleditorg=new Label(this, SWT.NONE);
		lbleditorg.setText("Edit Organization Detail");
		lbleditorg.setFont(new Font(display, "Times New Roman", 14, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(40);
		lbleditorg.setLayoutData(layout);
		
		lblsavemsg = new Label(this, SWT.NONE);
		lblsavemsg.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD|SWT.COLOR_RED));
		layout = new FormData();
		layout.top = new FormAttachment(lbleditorg,2);
		layout.left = new FormAttachment(40);
		layout.right = new FormAttachment(93);
		//layout.bottom = new FormAttachment(18);
		lblsavemsg.setLayoutData(layout);
		
		
		lblRegiNo = new Label(this, SWT.NONE);
		lblRegiNo.setText("Registration Num&ber :");
		lblRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(20);
		layout.bottom = new FormAttachment(28);
		lblRegiNo.setLayoutData(layout);
		
		txtRegiNo = new Text(this, SWT.BORDER);
		layout = new FormData();
		txtRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(26);
		layout.right = new FormAttachment(43);
		layout.bottom = new FormAttachment(28);
		txtRegiNo.setLayoutData(layout);
		
		lblFcraRegiNo = new Label(this, SWT.NONE);
		lblFcraRegiNo.setText("&FCRA Registration Number :");
		lblFcraRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(29);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(25);
		layout.bottom = new FormAttachment(33);
		lblFcraRegiNo.setLayoutData(layout);
		
		txtFcraRegiNo = new Text(this, SWT.BORDER);
		layout = new FormData();
		txtFcraRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout.top = new FormAttachment(29);
		layout.left = new FormAttachment(26);
		layout.right = new FormAttachment(43);
		layout.bottom = new FormAttachment(33);
		txtFcraRegiNo.setLayoutData(layout);
		
		lblAddress = new Label(this, SWT.NONE);
		lblAddress.setText("A&ddress :");
		lblAddress.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(34);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(11);
		layout.bottom = new FormAttachment(38);
		lblAddress.setLayoutData(layout);
		
		txtAddress = new Text(this, SWT.BORDER);
	    layout = new FormData();
		txtAddress.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout.top = new FormAttachment(34);
		layout.left = new FormAttachment(26);
		layout.right = new FormAttachment(43);
		layout.bottom = new FormAttachment(38);
		txtAddress.setLayoutData(layout);
		
		lblCountry = new Label(this, SWT.NONE);
		lblCountry.setText("&Country :");
		lblCountry.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(39);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(11);
		layout.bottom = new FormAttachment(43);
		lblCountry.setLayoutData(layout);
		
		dropdownCountry = new CCombo(this, SWT.BORDER | SWT.DROP_DOWN |SWT.READ_ONLY);
		layout = new FormData(); 
		dropdownCountry.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout.top = new FormAttachment(39);
		layout.left = new FormAttachment(26);
		layout.right = new FormAttachment(43);
		layout.bottom = new FormAttachment(43);
		dropdownCountry.add("----Please Select----");
		dropdownCountry.add("INDIA");
		dropdownCountry.setLayoutData(layout);
		dropdownCountry.select(0);
		
		lblState = new Label(this, SWT.NONE);
		lblState.setText("Stat&e :");
		lblState.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(44);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(9);
		layout.bottom = new FormAttachment(48);
		lblState.setLayoutData(layout);
		
		dropdownState = new CCombo(this, SWT.BORDER |SWT.READ_ONLY | SWT.DROP_DOWN | SWT.V_SCROLL);
		layout = new FormData(); 
		dropdownState.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout.top = new FormAttachment(44);
		layout.left = new FormAttachment(26);
		layout.right = new FormAttachment(43);
		layout.bottom = new FormAttachment(48);
		dropdownState.setLayoutData(layout);
		//dropdownState.add("----Please Select----",0);
		dropdownState.setEnabled(false);
		
		/*String[] states = StartupController.getStates();
		for (int i = 0; i < states.length; i++ )
		{
			dropdownState.add(states[i]);
			
		}*/
		/*if(dropdownCountry.getSelectionIndex()>0)
		{
			dropdownState.setEnabled(true);
			
		}
		else //if(dropdownCountry.getSelectionIndex()>0)
		{
			dropdownState.setEnabled(false);
		}*/

	
		//dropdownState.setItems(states);
		
		lblCity = new Label(this, SWT.NONE);
		lblCity.setText("Cit&y :");
		lblCity.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(49);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(9);
		layout.bottom = new FormAttachment(53);
		lblCity.setLayoutData(layout);
		
		dropdownCity = new CCombo(this, SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
		layout = new FormData();
		dropdownCity.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout.top = new FormAttachment(49);
		layout.left = new FormAttachment(26);
		layout.right = new FormAttachment(43);
		layout.bottom = new FormAttachment(53);
		dropdownCity.setLayoutData(layout);
		dropdownCity.add("----Please Select----",0);
		dropdownCity.setEnabled(false);
		
				

		

		
		
		lblPostalCode = new Label(this, SWT.NONE);
		lblPostalCode.setText("Posta&l Code :");
		lblPostalCode.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(54);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(13);
		layout.bottom = new FormAttachment(58);
		lblPostalCode.setLayoutData(layout);
		
		txtPostalCode = new Text(this, SWT.BORDER);
		layout = new FormData();
		txtPostalCode.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout.top = new FormAttachment(54);
		layout.left = new FormAttachment(26);
		layout.right = new FormAttachment(43);
		layout.bottom = new FormAttachment(58);
	    txtPostalCode.setLayoutData(layout);
		
	    lblvatNo = new Label(this, SWT.NONE);
		lblvatNo.setText("&VAT No :");
		lblvatNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(59);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(12);
		layout.bottom = new FormAttachment(63);
		lblvatNo.setLayoutData(layout);
		
		txtvatNo = new Text(this, SWT.BORDER);
		layout = new FormData();
		txtvatNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout.top = new FormAttachment(59);
		layout.left = new FormAttachment(26);
		layout.right = new FormAttachment(43);
		layout.bottom = new FormAttachment(63);
		txtvatNo.setLayoutData(layout);
		
		lblDtOfRegiNo = new Label(this, SWT.NONE);
		lblDtOfRegiNo.setText("Date &Of Registration :");
		lblDtOfRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(73);
		layout.bottom = new FormAttachment(28);
		lblDtOfRegiNo.setLayoutData(layout);
		
		txtDtDOrg = new Text(this, SWT.BORDER);
		txtDtDOrg.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtDtDOrg.setMessage("dd");
		txtDtDOrg.setTextLimit(2);
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(79);
		//layout.right = new FormAttachment(82);
		//layout.bottom = new FormAttachment(28);
		txtDtDOrg.setLayoutData(layout);
		
		lblDtOfRegiNoDash1 = new Label(this, SWT.NONE);
		lblDtOfRegiNoDash1.setText("-");
		lblDtOfRegiNoDash1.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(txtDtDOrg,2);
		lblDtOfRegiNoDash1.setLayoutData(layout);
		lblDtOfRegiNoDash1.setVisible(true);
		
		txtDtMOrg = new Text(this, SWT.BORDER);
		txtDtMOrg.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtDtMOrg.setMessage("mm");
		txtDtMOrg.setTextLimit(2);
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(lblDtOfRegiNoDash1,2);
		//layout.right = new FormAttachment();
		//layout.bottom = new FormAttachment(44);
		txtDtMOrg.setLayoutData(layout);
		
		lblDtOfRegiNoDash2 = new Label(this, SWT.NONE);
		lblDtOfRegiNoDash2.setText("-");
		lblDtOfRegiNoDash2.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(txtDtMOrg,2);
		lblDtOfRegiNoDash2.setLayoutData(layout);
		lblDtOfRegiNoDash2.setVisible(true);
		
		txtDtYOrg = new Text(this, SWT.BORDER);
		txtDtYOrg.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtDtYOrg.setMessage("yyyy");
		txtDtYOrg.setTextLimit(4);
		layout = new FormData();
		layout.top = new FormAttachment(24);
		layout.left = new FormAttachment(lblDtOfRegiNoDash2,2);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(44);
		txtDtYOrg.setLayoutData(layout);
		
		lblDtOfFcraRegiNo = new Label(this, SWT.NONE);
		lblDtOfFcraRegiNo.setText("Date Of FCRA Registratio&n :");
		lblDtOfFcraRegiNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(29);
		layout.left = new FormAttachment(50);
		lblDtOfFcraRegiNo.setLayoutData(layout);
		
		txtDtDOfFcraRegiNo = new Text(this, SWT.BORDER);
		txtDtDOfFcraRegiNo.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtDtDOfFcraRegiNo.setMessage("dd");
		txtDtDOfFcraRegiNo.setTextLimit(2);
		layout = new FormData();
		layout.top = new FormAttachment(29);
		layout.left = new FormAttachment(79);
		txtDtDOfFcraRegiNo.setLayoutData(layout);
		
		lblDtOfFcraRegiNoDash1 = new Label(this, SWT.NONE);
		lblDtOfFcraRegiNoDash1.setText("-");
		lblDtOfFcraRegiNoDash1.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(29);
		layout.left = new FormAttachment(txtDtDOfFcraRegiNo,2);
		lblDtOfFcraRegiNoDash1.setLayoutData(layout);
		lblDtOfFcraRegiNoDash1.setVisible(true);
		
		txtDtMOfFcraRegiNo = new Text(this, SWT.BORDER);
		txtDtMOfFcraRegiNo.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtDtMOfFcraRegiNo.setMessage("mm");
		txtDtMOfFcraRegiNo.setTextLimit(2);
		layout = new FormData();
		layout.top = new FormAttachment(29);
		layout.left = new FormAttachment(lblDtOfFcraRegiNoDash1,2);
		txtDtMOfFcraRegiNo.setLayoutData(layout);
		
		lblDtOfFcraRegiNoDash2 = new Label(this, SWT.NONE);
		lblDtOfFcraRegiNoDash2.setText("-");
		lblDtOfFcraRegiNoDash2.setFont(new Font(display, "Time New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(29);
		layout.left = new FormAttachment(txtDtMOfFcraRegiNo,2);
		lblDtOfFcraRegiNoDash2.setLayoutData(layout);
		lblDtOfFcraRegiNoDash2.setVisible(true);
		
		txtDtYOfFcraRegiNo = new Text(this, SWT.BORDER);
		txtDtYOfFcraRegiNo.setFont(new Font(display,"Times New Romen",7,SWT.NONE));
		txtDtYOfFcraRegiNo.setMessage("yyyy");
		txtDtYOfFcraRegiNo.setTextLimit(4);
		layout = new FormData();
		layout.top = new FormAttachment(29);
		layout.left = new FormAttachment(lblDtOfFcraRegiNoDash2,2);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(44);
		txtDtYOfFcraRegiNo.setLayoutData(layout);
		
		lblEmailId = new Label(this, SWT.NONE);
		lblEmailId.setText("Email&-Id :");
		lblEmailId.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(34);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(62);
		layout.bottom = new FormAttachment(38);
		lblEmailId.setLayoutData(layout);
		
		txtEmailId = new Text(this, SWT.BORDER);
		txtEmailId.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(34);
		layout.left = new FormAttachment(79);
		layout.right = new FormAttachment(96);
		layout.bottom = new FormAttachment(38);
		txtEmailId.setLayoutData(layout);
		
		lblTeliphoneNo = new Label(this, SWT.NONE);
		lblTeliphoneNo.setText("Telephone N&umber :");
		lblTeliphoneNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(39);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(74);
		layout.bottom = new FormAttachment(43);
		lblTeliphoneNo.setLayoutData(layout);
		
		txtTeliphoneNo = new Text(this, SWT.BORDER);
		txtTeliphoneNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(39);
		layout.left = new FormAttachment(79);
		layout.right = new FormAttachment(96);
		layout.bottom = new FormAttachment(43);
		txtTeliphoneNo.setLayoutData(layout);
		
		lblFaxNo = new Label(this, SWT.NONE);
		lblFaxNo.setText("Fa&x Number :");
		lblFaxNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(44);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(65);
		layout.bottom = new FormAttachment(48);
		lblFaxNo.setLayoutData(layout);
		
		txtFaxNo = new Text(this, SWT.BORDER);
		txtFaxNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(44);
		layout.left = new FormAttachment(79);
		layout.right = new FormAttachment(96);
		layout.bottom = new FormAttachment(48);
		txtFaxNo.setLayoutData(layout);
		
		lblWebsite = new Label(this, SWT.NONE);
		lblWebsite.setText("&Website :");
		lblWebsite.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(49);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(64);
		layout.bottom = new FormAttachment(53);
		lblWebsite.setLayoutData(layout);
		
		txtWebsite = new Text(this, SWT.BORDER);
		txtWebsite.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(49);
		layout.left = new FormAttachment(79);
		layout.right = new FormAttachment(96);
		layout.bottom = new FormAttachment(53);
		txtWebsite.setLayoutData(layout);

		lblPAN = new Label(this, SWT.NONE);
		lblPAN.setText("&Permanent Account Number :");
		lblPAN.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(54);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(77);
		layout.bottom = new FormAttachment(58);
		lblPAN.setLayoutData(layout);
		
		txtPAN = new Text(this, SWT.BORDER);
		txtPAN.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(54);
		layout.left = new FormAttachment(79);
		layout.right = new FormAttachment(96);
		layout.bottom = new FormAttachment(58);
		txtPAN.setLayoutData(layout);
		
		lblServiceTaxNo = new Label(this, SWT.NONE);
		lblServiceTaxNo.setText("Serv&ice Tax Number :");
		lblServiceTaxNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(59);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(76);
		layout.bottom = new FormAttachment(63);
		lblServiceTaxNo.setLayoutData(layout);
		
		txtServiceTaxNo = new Text(this, SWT.BORDER);
		txtServiceTaxNo.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(59);
		layout.left = new FormAttachment(79);
		layout.right = new FormAttachment(96);
		layout.bottom = new FormAttachment(63);
		txtServiceTaxNo.setLayoutData(layout);
		
		btnSave = new Button(this,SWT.PUSH);
		btnSave.setText("&Save");
		btnSave.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		btnSave.setToolTipText("Click here to save all the details and create the database.");
		layout = new FormData();
		layout.top = new FormAttachment(85);
		layout.left = new FormAttachment(40);
		layout.right = new FormAttachment(55);
		//layout.bottom = new FormAttachment(90);
		btnSave.setLayoutData(layout);

		
		
		
		/*String stradd=orgDetails[0].toString();
		MessageBox len = new MessageBox(new Shell(), SWT.OK);
		len.setMessage(stradd);
		len.open();
		*/
		
		final String[] orgDetails = StartupController.getOrgDetail();
		
		txtAddress.setText(orgDetails[2].toString());
		//dropdownCity.setText(orgDetails[3].toString());
		txtPostalCode.setText(orgDetails[4].toString());
		state=orgDetails[5].toString();
		//dropdownState.setText(orgDetails[5].toString());
		//dropdownCountry.setText(orgDetails[6].toString());
		//dropdownCity.setText(orgDetails[3].toString());
		txtTeliphoneNo.setText(orgDetails[7].toString());
		txtFaxNo.setText(orgDetails[8].toString());
		txtWebsite.setText(orgDetails[9].toString());
		txtEmailId.setText(orgDetails[10].toString());
		txtPAN.setText(orgDetails[11].toString());
		txtvatNo.setText(orgDetails[12].toString());
		txtServiceTaxNo.setText(orgDetails[13].toString());
		txtRegiNo.setText(orgDetails[14].toString());
		
		
		Display.getCurrent().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for (int i = 0; i < dropdownCountry.getItemCount(); i++) {
					if(dropdownCountry.getItem(i).equals(orgDetails[6].toString()))
							{
								dropdownCountry.select(i);
							}
				}
				if(dropdownCountry.getSelectionIndex()>0)
				{
				
					String[] states = StartupController.getStates();
					dropdownState.setItems(states);
					dropdownState.add("----Please Select----",0);
				}
				for (int i = 0; i < dropdownState.getItemCount(); i++) {
					if(dropdownState.getItem(i).equals(orgDetails[5].toString()))
							{
								dropdownState.select(i);
							}
				}
				
					String[] cities = StartupController.getCities(state);
					//String[] cities = StartupController.getCities(dropdownState.getItem(dropdownState.getSelectionIndex()));
					dropdownCity.setItems(cities);
					dropdownCity.add("----Please Select----",0);
					//dropdownCity.setItem(0, "----Please Select----");
				
				for (int i = 0; i < dropdownCity.getItemCount(); i++) {
					if(dropdownCity.getItem(i).equals(orgDetails[3].toString()))
							{
								dropdownCity.select(i);
							}
				}
				

			}
		});
		
		Display.getCurrent().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (dropdownCountry.getSelectionIndex()>0) {
					dropdownState.setEnabled(true);
				}
				else
				{
					dropdownState.setEnabled(false);
				}
				if (dropdownState.getSelectionIndex()>0) {
					dropdownCity.setEnabled(true);
				}
				else
				{
					dropdownCity.setEnabled(false);
				}
			}
		});
		
		try {
			if(orgDetails[15].toString().trim().equals(""))
			{
				System.out.println(orgDetails[15].toString());
				
			txtDtDOrg.setText("");
			txtDtMOrg.setText("");
			txtDtYOrg.setText("");
			
			}
			else
			{
			System.out.println(orgDetails[15].toString());
				
				txtDtDOrg.setText(orgDetails[15].toString().substring(0,2));
				txtDtMOrg.setText(orgDetails[15].toString().substring(3,5));
				txtDtYOrg.setText(orgDetails[15].toString().substring(6));
			}
			txtFcraRegiNo.setText(orgDetails[16].toString());
			if(orgDetails[17].toString().trim().equals(""))
			{
				System.out.println(orgDetails[17].toString());
				txtDtDOfFcraRegiNo.setText("");
				txtDtMOfFcraRegiNo.setText("");
				txtDtYOfFcraRegiNo.setText("");
			}
				else
			{
					System.out.println(orgDetails[17].toString());
			txtDtDOfFcraRegiNo.setText(orgDetails[17].toString().substring(0,2));
			txtDtMOfFcraRegiNo.setText(orgDetails[17].toString().substring(3,5));
			txtDtYOfFcraRegiNo.setText(orgDetails[17].toString().substring(6));
			}
		} catch (StringIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			System.out.println("yeda");
			System.out.println("ca"+orgDetails[15].toString());
			txtDtDOrg.setText("");
			txtDtMOrg.setText("");
			txtDtYOrg.setText("");
			
			txtDtDOfFcraRegiNo.setText("");
			txtDtMOfFcraRegiNo.setText("");
			txtDtYOfFcraRegiNo.setText("");
			e.printStackTrace();
		}
		
		
		
		
		
	/*	String[] cities = StartupController.getCities(state);
		//String[] cities = StartupController.getCities(dropdownState.getItem(dropdownState.getSelectionIndex()));
		dropdownCity.setItems(cities);
		dropdownCity.setText(orgDetails[3].toString());*/
		//txt.setText(orgDetails[17].toString());
		this.getAccessible();
		
		this.setEvents();
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
        BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		
		globals.setThemeColor(this, Background, Foreground);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        
	
		this.pack();
		
	}
	
	
	private void setEvents()
	{
		
		//the selection listenner is click event.
		//We are going to use adapters instead of listenners.
		//adapters are abstract classes so Eclipse allows us to override the methods.
		System.out.println("inside set events");
		try
		{
			txtRegiNo.addFocusListener(new FocusAdapter() {
				public void focusGained(FocusEvent arg0) {
					
				};
			});
			
			dropdownCountry.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//dropdownCity.removeAll();
					if(dropdownCountry.getSelectionIndex()==0)
					{
						dropdownState.removeAll();
						dropdownCity.removeAll();
						dropdownState.setEnabled(false);
						dropdownCity.setEnabled(false);
						//dropdownState.removeAll();
						//dropdownCity.removeAll();
						
					}
					else //if(dropdownCountry.getSelectionIndex()>0)
					{
						//dropdownState.add("----Please Select----",0);
						String[] states = StartupController.getStates();
					   // dropdownState.setItems(states);
						dropdownState.setItems(states);
						dropdownState.add("----Please Select----",0);
						
						dropdownState.setEnabled(true);
						
						//dropdownState.setItems(states);
					}
					
					/*if(dropdownCountry.getSelectionIndex()> 0)
					{
						dropdownState.setEnabled(true);
					String[] states = StartupController.getStates();
					//dropdownState.setItems(states);
					dropdownState.add("----Please Select----");
					for (int i = 0; i < states.length; i++ )
					{
						dropdownState.add(states[i]);
						
					}
					 //dropdownState.add("please select,0");
					//dropdownState.select(0);
				
					//dropdownState.setItems(states);
					
				}
					else 
					{
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//dropdownCity.removeAll();
						 if (dropdownCountry.getSelectionIndex()== 0) 
							{
							 //dropdownState.setItems(null);
							 dropdownState.add("");
							 dropdownCity.add(" ");
								//dropdownState.setEnabled(false);
							}
						 dropdownState.removeAll();
						 dropdownState.setEnabled(false);
						 dropdownCity.removeAll();
						 dropdownCity.setEnabled(false);
						
					
					}*/
				}});
			
			dropdownState.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//dropdownCity.removeAll();
					if(dropdownState.getSelectionIndex()> 0)
					{
						dropdownCity.setEnabled(true);
					String[] cities = StartupController.getCities(dropdownState.getItem(dropdownState.getSelectionIndex()));
					dropdownCity.setItems(cities);
					dropdownCity.add("----Please Select----",0);
					
				}
					else 
					{
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					//dropdownCity.removeAll();
						 
						 
						dropdownCity.setEnabled(false);
					
					}
				}});	
			btnSave.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
				/*	if((txtDtDOrg.getText().equals("") && txtDtMOrg.getText().equals("")&& txtDtYOrg.getText().equals("")) || (!(txtDtDOrg.getText().equals("")) && !(txtDtMOrg.getText().equals("")) && !(txtDtYOrg.getText().equals("")) || (txtDtDOfFcraRegiNo.getText().equals("") && txtDtMOfFcraRegiNo.getText().equals("")&& txtDtYOfFcraRegiNo.getText().equals("")) || (!(txtDtDOfFcraRegiNo.getText().equals("")) && !(txtDtMOfFcraRegiNo.getText().equals(""))&& !(txtDtYOfFcraRegiNo.getText().equals("")))))
					{
						if(!txtDtDOrg.getText().equals("") && (Integer.valueOf(txtDtDOrg.getText())> 31 || Integer.valueOf(txtDtDOrg.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid date");
							msgdateErr.open();
							
							txtDtDOrg.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOrg.setFocus();
									return;
								}
							});
							return;
						}
						
						if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtMOrg.getTextLimit())
						{	
							String dt = txtDtDOrg.getText();
							txtDtDOrg.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}	
						
						if(!txtDtMOrg.getText().equals("") && (Integer.valueOf(txtDtMOrg.getText())> 12 || Integer.valueOf(txtDtMOrg.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid Month");
							msgdateErr.open();
							
							txtDtMOrg.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOrg.setFocus();
									return;
								}
								
							});
							return;
						}
						
						if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtMOrg.getTextLimit())
						{	
							String dt = txtDtMOrg.getText();
							txtDtMOrg.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}	
						
						if(!txtDtDOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtDOfFcraRegiNo.getText())> 31 || Integer.valueOf(txtDtDOfFcraRegiNo.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid date");
							msgdateErr.open();
							
							txtDtDOfFcraRegiNo.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}}
				
						if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
						{	
							String dt = txtDtDOfFcraRegiNo.getText();
							txtDtDOfFcraRegiNo.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}	
						
						
						if(!txtDtMOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtMOfFcraRegiNo.getText())> 12 || Integer.valueOf(txtDtMOfFcraRegiNo.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid Month");
							msgdateErr.open();
							
							txtDtMOfFcraRegiNo.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOfFcraRegiNo.setFocus();
									return;
								}
								
							});
							return;
						}
						if(!txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
						{	
							String dt = txtDtMOfFcraRegiNo.getText();
							txtDtMOfFcraRegiNo.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}*/	
				/*	try {
						Date orgdate = sdf.parse(txtDtDOrg.getText() + "-" + txtDtMOrg.getText() + "-" + txtDtYOrg.getText());
						
						} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Invalid Date");
						txtDtDOrg.setFocus();
						msg.open();
						return;
					}
					
					try {
						Date fcradate = sdf.parse(txtDtDOfFcraRegiNo.getText()+ "-" + txtDtMOfFcraRegiNo.getText() + "-" + txtDtYOfFcraRegiNo.getText());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_ERROR);
						msg.setText("Error!");
						msg.setMessage("Invalid Date");
						txtDtDOfFcraRegiNo.setFocus();
						msg.open();
						return;
					}*/
					
					/*if(txtAddress.getText().trim().equals(""))
					{
						MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg.setText("Alert!!");
						msg.setMessage("Please enter address");
						msg.open();
						txtAddress.setFocus();
						return;
					}*/
					/*if(dropdownCountry.getSelectionIndex()<= -1 )
					
					{
						MessageBox msgvalidate = new MessageBox(new Shell(), SWT.OK |SWT.ICON_INFORMATION);
						msgvalidate.setText("Alert!!");
						msgvalidate.setMessage("Please select a valid country");
						msgvalidate.open();
						dropdownCountry.setFocus();
						return;
					}*/
					/*if(dropdownState.getSelectionIndex()<= 0 )
						
					{
						MessageBox msgvalidate = new MessageBox(new Shell(), SWT.OK | SWT.ICON_INFORMATION);
						msgvalidate.setMessage("Please select a valid state");
						msgvalidate.setText("Alert!!");
						msgvalidate.open();
						dropdownState.setFocus();
						return;
					}*/
					/*if(dropdownCity.getSelectionIndex()< 0 )
						
					{
						MessageBox msgvalidate = new MessageBox(new Shell(), SWT.OK | SWT.ICON_INFORMATION);
						msgvalidate.setText("Alert!!");
						msgvalidate.setMessage("Please select a valid city");
						msgvalidate.open();
						dropdownCity.setFocus();
						return;
					}*/
					/*if(txtPostalCode.getText().trim().equals(""))
					{
						MessageBox msg = new MessageBox(new Shell(),SWT.OK |SWT.ICON_INFORMATION);
						msg.setText("Alert!!");
						msg.setMessage("Please enter postalcode");
						msg.open();
						txtPostalCode.setFocus();
						return;
					}*/
					if((!txtDtDOrg.getText().trim().equals("")) || (!txtDtMOrg.getText().trim().equals("")) || (!txtDtYOrg.getText().trim().equals("")) ||((!txtDtDOfFcraRegiNo.getText().trim().equals("")) || (!txtDtMOfFcraRegiNo.getText().trim().equals("")) || (!txtDtYOfFcraRegiNo.getText().trim().equals(""))))
					{
						if(!txtDtDOrg.getText().equals("") && (Integer.valueOf(txtDtDOrg.getText())> 31 || Integer.valueOf(txtDtDOrg.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Error!!");
							msgdateErr.setMessage("You have entered an Invalid Date");
							msgdateErr.open();
							
							//txtDtDOrg.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOrg.setFocus();
								}
							});
							return;
						}
						if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtDOrg.getTextLimit())
						{
							txtDtDOrg.setText("0"+ txtDtDOrg.getText());
							//txtFromDtMonth.setFocus();
							
						}
						if((txtDtDOrg.getText().trim().equals("") && ! txtDtMOrg.getText().equals("") && ! txtDtYOrg.getText().equals("")) || ((txtDtDOrg.getText().trim().equals("") && (! txtDtMOrg.getText().equals("") || ! txtDtYOrg.getText().equals("")))))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a date in DD format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOrg.setFocus();
									
								}
							});
							return;
						}
						if(!txtDtMOrg.getText().equals("") && (Integer.valueOf(txtDtMOrg.getText())> 12 || Integer.valueOf(txtDtMOrg.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Error!");
							msgdateErr.setMessage("You have entered an Invalid Month.");
							msgdateErr.open();
							
							
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									//txtDtMOrg.setText("");
									txtDtMOrg.setFocus();
									txtDtMOrg.selectAll();
									
								}
							});
							return;
						}
						if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtDOrg.getTextLimit())
						{
							txtDtMOrg.setText("0"+ txtDtMOrg.getText());
							//txtFromDtMonth.setFocus();
							
						}
						if((txtDtDOrg.getText().equals("") && txtDtMOrg.getText().trim().equals("") && !txtDtYOrg.getText().equals(""))||((!txtDtDOrg.getText().equals("") || !txtDtYOrg.getText().equals("")) && txtDtMOrg.getText().trim().equals("")))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Month in MM format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOrg.setFocus();
									
								}
							});
							return;
						}
						if(!txtDtYOrg.getText().trim().equals("")&& (Integer.valueOf(txtDtYOrg.getText())> 2100 || Integer.valueOf(txtDtYOrg.getText()) < 1900) ) 
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
						    msgDayErr.open();
							//txtDtYOrg.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOrg.setFocus();
									txtDtYOrg.selectAll();
									
								}
							});
							return;
						}
						if(!txtDtYOrg.getText().equals("") && Integer.valueOf ( txtDtYOrg.getText())<10 && txtDtYOrg.getText().length()< txtDtYOrg.getTextLimit())
						{
							txtDtYOrg.setText("0"+ txtDtYOrg.getText());
							//txtFromDtMonth.setFocus();
							//txtFcraRegiNo.setFocus();
							
						}
						if((!txtDtDOrg.getText().equals("") || !txtDtMOrg.getText().equals("")) && txtDtYOrg.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOrg.setFocus();
									
								}
							});
							return;
						}
						if(!txtDtDOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtDOfFcraRegiNo.getText())> 31 || Integer.valueOf(txtDtDOfFcraRegiNo.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Error!");
							msgdateErr.setMessage("You have entered an Invalid Date.");
							msgdateErr.open();
							
							txtDtDOfFcraRegiNo.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOfFcraRegiNo.setFocus();
									
								}
							});
							return;
						}
						if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
						{
							txtDtDOfFcraRegiNo.setText("0"+ txtDtDOfFcraRegiNo.getText());
							//txtFromDtMonth.setFocus();
						}
						if((txtDtDOfFcraRegiNo.getText().trim().equals("") && ! txtDtMOfFcraRegiNo.getText().equals("") && ! txtDtYOfFcraRegiNo.getText().equals("")) || ((txtDtDOfFcraRegiNo.getText().trim().equals("") && (! txtDtMOfFcraRegiNo.getText().equals("") || ! txtDtYOfFcraRegiNo.getText().equals("")))))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a date in DD format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}
						if(!txtDtMOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtMOfFcraRegiNo.getText())> 12 || Integer.valueOf(txtDtMOfFcraRegiNo.getText()) <= 0))
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Error!");
							msgdateErr.setMessage("You have entered an Invalid Month.");
							msgdateErr.open();
							
							
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									//txtDtMOfFcraRegiNo.setText("");
									txtDtMOfFcraRegiNo.setFocus();
									txtDtMOfFcraRegiNo.selectAll();
									
								}
							});
							return;
							
						}
						if(! txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
						{
							txtDtMOfFcraRegiNo.setText("0"+ txtDtMOfFcraRegiNo.getText());
						}
						if((!txtDtDOfFcraRegiNo.getText().equals("") && txtDtMOfFcraRegiNo.getText().trim().equals("") && !txtDtYOfFcraRegiNo.getText().equals(""))||((!txtDtDOfFcraRegiNo.getText().equals("") || !txtDtYOfFcraRegiNo.getText().equals("")) && txtDtMOfFcraRegiNo.getText().trim().equals("")))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Month in MM format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}
						if(!txtDtYOfFcraRegiNo.getText().trim().equals("")&& (Integer.valueOf(txtDtYOfFcraRegiNo.getText())> 2100 || Integer.valueOf(txtDtYOfFcraRegiNo.getText()) < 1900) )
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
							msgDayErr.open();
						//	txtDtYOfFcraRegiNo.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									txtDtYOfFcraRegiNo.selectAll();
									
								}
							});
							return;
						}
						if(!txtDtYOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtYOfFcraRegiNo.getText())<10 && txtDtYOfFcraRegiNo.getText().length()< txtDtYOfFcraRegiNo.getTextLimit())
						{
							txtDtYOfFcraRegiNo.setText("0"+ txtDtYOfFcraRegiNo.getText());
							//txtFromDtMonth.setFocus();
							//txtFcraRegiNo.setFocus();
							
						}
						
						if((!txtDtDOfFcraRegiNo.getText().equals("") || !txtDtMOfFcraRegiNo.getText().equals("")) && txtDtYOfFcraRegiNo.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}

						}
					
					/*if((!txtDtDOfFcraRegiNo.getText().trim().equals("")) || (!txtDtMOfFcraRegiNo.getText().trim().equals("")) || (!txtDtYOfFcraRegiNo.getText().trim().equals("")))
					{
						if(!txtDtDOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtDOfFcraRegiNo.getText())> 31 || Integer.valueOf(txtDtDOfFcraRegiNo.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Error!");
							msgdateErr.setMessage("You have entered an Invalid Date.");
							msgdateErr.open();
							
							txtDtDOfFcraRegiNo.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOfFcraRegiNo.setFocus();
									
								}
							});
							return;
						}
						if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
						{
							txtDtDOfFcraRegiNo.setText("0"+ txtDtDOfFcraRegiNo.getText());
							//txtFromDtMonth.setFocus();
						}
						if((txtDtDOfFcraRegiNo.getText().trim().equals("") && ! txtDtMOfFcraRegiNo.getText().equals("") && ! txtDtYOfFcraRegiNo.getText().equals("")) || ((txtDtDOfFcraRegiNo.getText().trim().equals("") && (! txtDtMOfFcraRegiNo.getText().equals("") || ! txtDtYOfFcraRegiNo.getText().equals("")))))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a date in DD format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}
						if(!txtDtMOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtMOfFcraRegiNo.getText())> 12 || Integer.valueOf(txtDtMOfFcraRegiNo.getText()) <= 0))
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Error!");
							msgdateErr.setMessage("You have entered an Invalid Month.");
							msgdateErr.open();
							
							
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									//txtDtMOfFcraRegiNo.setText("");
									txtDtMOfFcraRegiNo.setFocus();
									txtDtMOfFcraRegiNo.selectAll();
									
								}
							});
							return;
							
						}
						if(! txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
						{
							txtDtMOfFcraRegiNo.setText("0"+ txtDtMOfFcraRegiNo.getText());
						}
						if((!txtDtDOfFcraRegiNo.getText().equals("") && txtDtMOfFcraRegiNo.getText().trim().equals("") && !txtDtYOfFcraRegiNo.getText().equals(""))||((!txtDtDOfFcraRegiNo.getText().equals("") || !txtDtYOfFcraRegiNo.getText().equals("")) && txtDtMOfFcraRegiNo.getText().trim().equals("")))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Month in MM format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}
						if(!txtDtYOfFcraRegiNo.getText().trim().equals("")&& Integer.valueOf(txtDtYOfFcraRegiNo.getText())<1990)
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Error!");
							msgDayErr.setMessage("You have entered an Invalid Year.");
							msgDayErr.open();
						//	txtDtYOfFcraRegiNo.setText("");
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									txtDtYOfFcraRegiNo.selectAll();
									
								}
							});
							return;
						}
						if(!txtDtYOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtYOfFcraRegiNo.getText())<10 && txtDtYOfFcraRegiNo.getText().length()< txtDtYOfFcraRegiNo.getTextLimit())
						{
							txtDtYOfFcraRegiNo.setText("0"+ txtDtYOfFcraRegiNo.getText());
							//txtFromDtMonth.setFocus();
							//txtFcraRegiNo.setFocus();
							
						}
						
						if((!txtDtDOfFcraRegiNo.getText().equals("") || !txtDtMOfFcraRegiNo.getText().equals("")) && txtDtYOfFcraRegiNo.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}
				

					
						
					}*/
					
					
					/*if(txtDtDOrg.getText().trim().equals("")&&!txtDtMOrg.getText().trim().equals("")&&!txtDtYOrg.getText().trim().equals("")||!txtDtDOrg.getText().trim().equals("")&&txtDtMOrg.getText().trim().equals("")&&!txtDtYOrg.getText().trim().equals("")||!txtDtDOrg.getText().trim().equals("")&&!txtDtMOrg.getText().trim().equals("")&&txtDtYOrg.getText().trim().equals(""))
					{
						txtDtDOrg.setText("");
						txtDtMOrg.setText("");
						txtDtYOrg.setText("");
					}
			
					if(txtDtDOrg.getText().trim().equals("")&&txtDtMOrg.getText().trim().equals("")&&!txtDtYOrg.getText().trim().equals("")||!txtDtDOrg.getText().trim().equals("")&&txtDtMOrg.getText().trim().equals("")&&txtDtYOrg.getText().trim().equals("")||txtDtDOrg.getText().trim().equals("")&&!txtDtMOrg.getText().trim().equals("")&&txtDtYOrg.getText().trim().equals(""))
					{
						txtDtDOrg.setText("");
						txtDtMOrg.setText("");
						txtDtYOrg.setText("");
					}
					
					if(txtDtDOrg.getText().trim().equals("")&&txtDtMOrg.getText().trim().equals("")&&txtDtYOrg.getText().trim().equals(""))
					{
						txtDtDOrg.setText("");
						txtDtMOrg.setText("");
						txtDtYOrg.setText("");
					}
			
			
					if(txtDtDOfFcraRegiNo.getText().trim().equals("")&&!txtDtMOfFcraRegiNo.getText().trim().equals("")&&!txtDtYOfFcraRegiNo.getText().trim().equals("")||!txtDtDOrg.getText().trim().equals("")&&txtDtMOrg.getText().trim().equals("")&&!txtDtYOrg.getText().trim().equals("")||!txtDtDOrg.getText().trim().equals("")&&!txtDtMOrg.getText().trim().equals("")&&txtDtYOrg.getText().trim().equals(""))
					{
						txtDtDOfFcraRegiNo.setText("");
						txtDtMOfFcraRegiNo.setText("");
						txtDtYOfFcraRegiNo.setText("");
					}
					
					if(txtDtDOfFcraRegiNo.getText().trim().equals("")&&txtDtMOfFcraRegiNo.getText().trim().equals("")&&!txtDtYOfFcraRegiNo.getText().trim().equals("")||!txtDtDOfFcraRegiNo.getText().trim().equals("")&&txtDtMOfFcraRegiNo.getText().trim().equals("")&&txtDtYOfFcraRegiNo.getText().trim().equals("")||txtDtDOfFcraRegiNo.getText().trim().equals("")&&!txtDtMOfFcraRegiNo.getText().trim().equals("")&&txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
						txtDtDOfFcraRegiNo.setText("");
						txtDtMOfFcraRegiNo.setText("");
						txtDtYOfFcraRegiNo.setText("");
					}*/
					
				/*	if(txtDtDOfFcraRegiNo.getText().trim().equals("")&&txtDtMOfFcraRegiNo.getText().trim().equals("")&&txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
						txtDtDOfFcraRegiNo.setText("");
						txtDtMOfFcraRegiNo.setText("");
						txtDtYOfFcraRegiNo.setText("");
					}*/
					//if((txtDtDOrg.getText().equals("") && txtDtMOrg.getText().equals("")&& txtDtYOrg.getText().equals("")) || (!txtDtDOrg.equals("")) && !(txtDtMOrg.getText().equals("")) && !(txtDtYOrg.getText().equals("")) ||(txtDtDOfFcraRegiNo.getText().equals("") && txtDtMOfFcraRegiNo.getText().equals("")&& txtDtYOfFcraRegiNo.getText().equals("")) || (!txtDtDOfFcraRegiNo.getText().equals("")) && !(txtDtMOfFcraRegiNo.getText().equals("")) && !(txtDtYOfFcraRegiNo.getText().equals("")))		
				
					ArrayList<String> paramsOrg=new ArrayList<String>();
					paramsOrg.add(strOrgName);
					paramsOrg.add(txtAddress.getText());
					if(dropdownCity.getItemCount() >= 0 && dropdownCity.getSelectionIndex() >= 0)
					{
						paramsOrg.add(dropdownCity.getItem(dropdownCity.getSelectionIndex()));
					}
					else
					{
						paramsOrg.add(" ");
					}
					//paramsOrg.add(dropdownCity.getItem(dropdownCity.getSelectionIndex()));
					paramsOrg.add(txtPostalCode.getText());
					if(dropdownState.getItemCount() > 0 && dropdownState.getSelectionIndex() > 0)
					{
						paramsOrg.add(dropdownState.getItem(dropdownState.getSelectionIndex()));
					}
					else
					{
						paramsOrg.add(" ");
					}
					//paramsOrg.add(dropdownState.getItem(dropdownState.getSelectionIndex()));
					if(dropdownCountry.getItemCount() > 0 && dropdownCountry.getSelectionIndex() > 0)
					{
						paramsOrg.add(dropdownCountry.getItem(dropdownCountry.getSelectionIndex()));
					}
					else
					{
						paramsOrg.add("----Please Select----");
					}
					//paramsOrg.add(dropdownCountry.getItem(dropdownCountry.getSelectionIndex()));
					paramsOrg.add(txtTeliphoneNo.getText());
					paramsOrg.add(txtFaxNo.getText());
					paramsOrg.add(txtWebsite.getText());
					paramsOrg.add(txtEmailId.getText());
					paramsOrg.add(txtPAN.getText());
					paramsOrg.add(txtvatNo.getText());
					paramsOrg.add(txtServiceTaxNo.getText());
					paramsOrg.add(txtRegiNo.getText());
					
					
				//	});
				
						/*if(txtDtDOrg.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a valid Date.");
							msgDayErr.open();
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									txtDtMOrg.setFocus();
								}
							});
							return;
						}*/
						
					
					
					if(txtDtDOrg.getText().trim().equals("")&&txtDtMOrg.getText().trim().equals("")&&txtDtYOrg.getText().trim().equals(""))
					{
						paramsOrg.add(""+""+""+""+"");
					}
					else
					{
						paramsOrg.add(txtDtDOrg.getText() + "-" + txtDtMOrg.getText() + "-" + txtDtYOrg.getText());

					}
					System.out.println(txtDtDOrg.getText() + "-" + txtDtMOrg.getText() + "-" + txtDtYOrg.getText());	paramsOrg.add(txtFcraRegiNo.getText());
					
					if(txtDtDOfFcraRegiNo.getText().trim().equals("")&&txtDtMOfFcraRegiNo.getText().trim().equals("")&&txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
						paramsOrg.add(""+""+""+""+"");
					}
					else
					{
						paramsOrg.add(txtDtDOfFcraRegiNo.getText()+ "-" + txtDtMOfFcraRegiNo.getText() + "-" + txtDtYOfFcraRegiNo.getText());
					}	
					System.out.println("fcra"+txtDtDOfFcraRegiNo.getText()+ "-" + txtDtMOfFcraRegiNo.getText() + "-" + txtDtYOfFcraRegiNo.getText());
					
					MessageBox msgConfirm = new MessageBox(new Shell(), SWT.YES| SWT.NO| SWT.ICON_QUESTION );
					msgConfirm.setText("Confirm ?");
					msgConfirm.setMessage("Do you want to save Changes ?");
					int answer = msgConfirm.open();
					if(answer == SWT.YES)
					{
						if(StartupController.updateOrgDetail(paramsOrg))
						{
							lblsavemsg.setText("Organisation updated Successfully.");
							lblsavemsg.setVisible(true);
						/*MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg.setText("Information!");
						msg.setMessage("Organisation updated Successfully.");
						msg.open();*/
						}
						if(! lblsavemsg.getText().equals(""))
						{
							Display.getCurrent().asyncExec(new Runnable(){
								public void run()
								{
									long now = System.currentTimeMillis();
									long lblTimeOUt = 0;
									while(lblTimeOUt < (now + 1000))
									{
										lblTimeOUt = System.currentTimeMillis();
									}
							
									//lblsavemsg.setText("");
									btnSave.getShell().getDisplay().dispose();
									MainShell ms = new MainShell(display);

								}
						});

							
						}

						/*btnSave.getShell().getDisplay().dispose();
						MainShell ms = new MainShell(display);
					*/
					}
					else if(answer == SWT.NO) 
					{
						txtRegiNo.setFocus();
					}
					/*if((txtDtDOrg.getText().equals("") && txtDtMOrg.getText().equals("")&& txtDtYOrg.getText().equals("")) || (!(txtDtDOrg.getText().equals("")) && !(txtDtMOrg.getText().equals("")) && !(txtDtYOrg.getText().equals("")) || (txtDtDOfFcraRegiNo.getText().equals("") && txtDtMOfFcraRegiNo.getText().equals("")&& txtDtYOfFcraRegiNo.getText().equals("")) || (!(txtDtDOfFcraRegiNo.getText().equals("")) && !(txtDtMOfFcraRegiNo.getText().equals(""))&& !(txtDtYOfFcraRegiNo.getText().equals("")))))
					{
						if(!txtDtDOrg.getText().equals("") && (Integer.valueOf(txtDtDOrg.getText())> 31 || Integer.valueOf(txtDtDOrg.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid date");
							msgdateErr.open();
							
							txtDtDOrg.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOrg.setFocus();
									return;
								}
							});
							return;
						}
						
						if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtMOrg.getTextLimit())
						{	
							String dt = txtDtDOrg.getText();
							txtDtDOrg.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}	
						
						if(!txtDtMOrg.getText().equals("") && (Integer.valueOf(txtDtMOrg.getText())> 12 || Integer.valueOf(txtDtMOrg.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid Month");
							msgdateErr.open();
							
							txtDtMOrg.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOrg.setFocus();
									return;
								}
								
							});
							return;
						}
						
						if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtMOrg.getTextLimit())
						{	
							String dt = txtDtMOrg.getText();
							txtDtMOrg.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}	
						
						if(!txtDtDOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtDOfFcraRegiNo.getText())> 31 || Integer.valueOf(txtDtDOfFcraRegiNo.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid date");
							msgdateErr.open();
							
							txtDtDOfFcraRegiNo.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}}
				
						if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
						{	
							String dt = txtDtDOfFcraRegiNo.getText();
							txtDtDOfFcraRegiNo.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}	
						
						
						if(!txtDtMOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtMOfFcraRegiNo.getText())> 12 || Integer.valueOf(txtDtMOfFcraRegiNo.getText()) <= 0) )
						{
							MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgdateErr.setText("Validation Date Error!");
							msgdateErr.setMessage("You have entered an invalid Month");
							msgdateErr.open();
							
							txtDtMOfFcraRegiNo.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOfFcraRegiNo.setFocus();
									return;
								}
								
							});
							return;
						}
						if(!txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
						{	
							String dt = txtDtMOfFcraRegiNo.getText();
							txtDtMOfFcraRegiNo.setText("0"+dt);
							//txtFromDtMonth.setFocus();
							
							//txtDtYOrg.setFocus();
							return;
						}*/	
					
				
				/*	else
					{
						if(txtDtDOrg.getText().trim().equals("") )
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a date in DD format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOrg.setFocus();
									
								}
							});
							return;
						}
						
						
						if(txtDtMOrg.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Month in MM format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOrg.setFocus();
									
								}
							});
							return;
						}
						
					
						/*if(txtDtYOrg.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOrg.setFocus();
									
								}
							});
							return;
						}
						

						/*if(txtDtDOfFcraRegiNo.getText().trim().equals("") )
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a date in DD format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtDOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}
						
						
						/*if(txtDtMOfFcraRegiNo.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Month in MM format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtMOfFcraRegiNo.setFocus();
									return;
								}
							});
							return;
						}
						
					
						/*if(txtDtYOfFcraRegiNo.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
							msgDayErr.open();
							
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOfFcraRegiNo.setFocus();
									
								}
							});
							return;
						
					
						
					}
		
				
					/*txtRegiNo.setText("");
					txtFcraRegiNo.setText("");
					txtDtDOrg.setText("");
					txtDtMOrg.setText("");
					txtDtYOrg.setText("");
					txtDtDOfFcraRegiNo.setText("");
					txtDtMOfFcraRegiNo.setText("");
					txtDtYOfFcraRegiNo.setText("");
					txtAddress.setText("");
					dropdownCity.select(0);
					dropdownCountry.select(0);
					dropdownState.select(0);
					txtPostalCode.setText("");
					txtMvatNo.setText("");
					txtEmailId.setText("");
					txtTeliphoneNo.setText("");
					txtFaxNo.setText("");
					txtWebsite.setText("");
					txtPAN.setText("");
					txtServiceTaxNo.setText("");
				}*/
				}});
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		txtRegiNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR)
				{
					txtDtDOrg.setFocus();
				}

			}
		});
		
		txtFcraRegiNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR)
				{
					txtDtDOfFcraRegiNo.setFocus();
				}
			if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtDtYOrg.setFocus();
				}

			}
		});
		
	/*	txtFcraRegiNo.addFocusListener(new FocusAdapter() {
			
			public void focusGained(FocusEvent arg0) {
				 
				// TODO Auto-generated method stub
						
						if(!txtDtYOrg.getText().trim().equals("") && Integer.valueOf(txtDtYOrg.getText()) < 1900)
								{
							MessageBox msgbox = new MessageBox(new Shell(), SWT.OK |SWT.ERROR | SWT.ICON_ERROR);
							msgbox.setText("Validation Year Error!");
							msgbox.setMessage("You have entered an Invalid Year");
							msgbox.open();
						
							txtDtYOrg.setText("");
							Display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtDtYOrg.setFocus();
									txtDtYOrg.selectAll();
									
								}
								
								
							});
							return;
						}
				/*		if(txtDtDOrg.getText().trim().equals("")&&txtDtMOrg.getText().trim().equals("")&&txtDtYOrg.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a valid Date.");
							msgDayErr.open();
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									txtDtDOrg.setFocus();
								}
							});
							return;
						}*/
						/*if(txtDtDOrg.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a valid Date.");
							msgDayErr.open();
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									txtDtDOrg.setFocus();
								}
							});
							return;
						}
						if(txtDtMOrg.getText().trim().equals(""))
						{
							MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
							msgDayErr.setText("Validation Date Error!");
							msgDayErr.setMessage("Please enter a valid Date.");
							msgDayErr.open();
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									txtDtMOrg.setFocus();
								}
							});
							return;
						}}});*/

				/*try{
				if(Integer.valueOf(txtDtDOrg.getText()) > 0||Integer.valueOf(txtDtMOrg.getText()) > 0)
				{
				if(txtDtDOrg.getText().trim().equals("")&&!txtDtDOrg.getText().trim().equals("")&&!txtDtDOrg.getText().trim().equals(""))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR |SWT.ICON_ERROR);
					msgDayErr.setText("Validation date Error!");
					msgDayErr.setMessage("Please Enter Valid Date in dd format.");
					msgDayErr.open();
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtDOrg.setFocus();
							
						}
					});
					return;
				}}
				}}
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
			//	super.focusLost(arg0);
					}
			
		
			});*/
		
		txtAddress.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR)
				{
					dropdownCountry.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
				
					txtDtYOfFcraRegiNo.setFocus();
				}

			}
		});
		
		dropdownCountry.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR && dropdownCountry.getSelectionIndex()==0)
				{
					txtPostalCode.setFocus();
				}
				if(arg0.keyCode==SWT.CR)
				{
					dropdownState.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP && dropdownCountry.getSelectionIndex()==0)
				{
					txtAddress.setFocus();
				}

			}
		});
		
		dropdownCountry.addFocusListener(new FocusAdapter(){
			public void focusGained(FocusEvent arg0) {
				dropdownCountry.setListVisible(true);
			};
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				dropdownCountry.setListVisible(false);
			}
		});
		
		dropdownState.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR && dropdownState.getSelectionIndex()==0)
				{
					txtPostalCode.setFocus();
				}
				if(arg0.keyCode==SWT.CR)
				{
					dropdownCity.setFocus();
					//dropdownCity.getItems();
				}
				if(arg0.keyCode==SWT.ARROW_UP && dropdownState.getSelectionIndex()==0)
				{
					dropdownCountry.setFocus();
				}
			}
		});
		
		dropdownState.addFocusListener(new FocusAdapter(){
			public void focusGained(FocusEvent arg0) {
				dropdownState.setListVisible(true);
			};
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				dropdownState.setListVisible(false);
			}
		});
		
		dropdownCity.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode == SWT.CR)
				{
					txtPostalCode.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP && dropdownCity.getSelectionIndex()==0)
				{
					dropdownState.setFocus();
				}
			}
		});
		
		dropdownCity.addFocusListener(new FocusAdapter(){
			public void focusGained(FocusEvent arg0) {
				dropdownCity.setListVisible(true);
			};
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				dropdownCity.setListVisible(false);
			}
		});
		
		txtPostalCode.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
				{
					arg0.doit = true;
				}
				else
				{						
					arg0.doit = false;
				}
			}
		});
		
		txtPostalCode.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR)
				{
					
					txtvatNo.setFocus();
				}			
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					dropdownCity.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP && dropdownState.getSelectionIndex()==0)
				{
					dropdownState.setFocus();
				}
				if(arg0.keyCode==SWT.ARROW_UP && dropdownCountry.getSelectionIndex()==0)
				{
					dropdownCountry.setFocus();
				}
				
			}
		});
		
		
		txtvatNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR)
				{
					txtEmailId.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtPostalCode.setFocus();
				}

			}
		});
		
		
		
		txtDtDOrg.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
				{
					//txtDtDOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
					if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtDOrg.getTextLimit())
					{
						txtDtDOrg.setText("0"+ txtDtDOrg.getText());
						//txtFromDtMonth.setFocus();
						txtDtMOrg.setFocus();
						return;
					}
					else
					{
						txtDtMOrg.setFocus();
					}
				}
				if(arg0.keyCode ==SWT.TAB)
				{
					if(txtDtDOrg.getText().equals(""))
					{
						txtDtDOrg.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOrg.setFocus();
							}
						});
						return;
					}
				}
				
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtRegiNo.setFocus();
				}
			}
		});
		
		txtDtMOrg.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
				{
					//txtDtMOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
					if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtMOrg.getTextLimit())
					{
						txtDtMOrg.setText("0"+ txtDtMOrg.getText());
						//txtFromDtMonth.setFocus();
						txtDtYOrg.setFocus();
						return;
					}
					else
					{
						txtDtYOrg.setFocus();
					}
				}
				if(arg0.keyCode ==SWT.TAB)
				{
					if(txtDtMOrg.getText().equals(""))
					{
						txtDtMOrg.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOrg.setFocus();
							}
						});
						return;
					}
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtDtDOrg.setFocus();
				}
			}
		});
		
		txtDtYOrg.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
				{
					//txtDtYOrg.traverse(SWT.TRAVERSE_TAB_NEXT);
					if(!txtDtYOrg.getText().equals("") && Integer.valueOf ( txtDtYOrg.getText())<10 && txtDtYOrg.getText().length()< txtDtYOrg.getTextLimit())
					{
						txtDtYOrg.setText("0"+ txtDtYOrg.getText());
						//txtFromDtMonth.setFocus();
						txtFcraRegiNo.setFocus();
						return;
					}
					else
					{
						txtFcraRegiNo.setFocus();
					}
					
					/*if((!txtDtDOrg.getText().equals("") || !txtDtMOrg.getText().equals("")) && txtDtYOrg.getText().trim().equals(""))
					{
						if(txtDtDOrg.getText().equals("") && !txtDtMOrg.getText().equals("") && txtDtYOrg.getText().trim().equals(""))
						{
							txtDtDOrg.setFocus();
							return;
						}
						if(!txtDtDOrg.getText().equals("") && txtDtMOrg.getText().equals("") && txtDtYOrg.getText().trim().equals(""))
						{
							txtDtMOrg.setFocus();
							return;
						}
						//if((!txtDtDOrg.getText().equals("") && !txtDtMOrg.getText().equals("")) ||((!txtDtDOrg.getText().equals("")  || !txtDtMOrg.getText().equals(""))))
						else
						{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOrg.setFocus();
								
							}
						});
						return;
						}}*/

				}
				if(arg0.keyCode ==SWT.TAB)
				{
					if(txtDtYOrg.getText().equals(""))
					{
						txtDtYOrg.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOrg.setFocus();
							}
						});
						return;
					}
				}
				if(arg0.keyCode==SWT.ARROW_UP)
				{
					txtDtMOrg.setFocus();
				}
				/*if(arg0.keyCode==SWT.ARROW_UP && txtDtYOrg.getText().equals(""))
				{
					txtDtMOrg.setForcefocus();
				
				}*/
				}
		});
		
txtDtDOrg.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
		
		
		
	txtDtMOrg.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
//				/*if(verifyFlag== false)
//				{
//					arg0.doit= true;
//					return;
//				}*/
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }
				if(arg0.keyCode==46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

			}
		});
	
	txtDtYOrg.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
//			/*if(verifyFlag== false)
//			{
//				arg0.doit= true;
//				return;
//			}*/
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	txtDtMOfFcraRegiNo.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
//			/*if(verifyFlag== false)
//			{
//				arg0.doit= true;
//				return;
//			}*/
			switch (arg0.keyCode) {
            case SWT.BS:           // Backspace
            case SWT.DEL:          // Delete
            case SWT.HOME:         // Home
            case SWT.END:          // End
            case SWT.ARROW_LEFT:   // Left arrow
            case SWT.ARROW_RIGHT:  // Right arrow
            case SWT.TAB:
            case SWT.CR:
            case SWT.KEYPAD_CR:
            case SWT.KEYPAD_DECIMAL:
                return;
        }
			if(arg0.keyCode==46)
			{
				return;
			}
        if (!Character.isDigit(arg0.character)) {
            arg0.doit = false;  // disallow the action
        }

		}
	});
	
txtDtDOfFcraRegiNo.addVerifyListener(new VerifyListener() {
	
	@Override
	public void verifyText(VerifyEvent arg0) {
		// TODO Auto-generated method stub
//		/*if(verifyFlag== false)
//		{
//			arg0.doit= true;
//			return;
//		}*/
		switch (arg0.keyCode) {
        case SWT.BS:           // Backspace
        case SWT.DEL:          // Delete
        case SWT.HOME:         // Home
        case SWT.END:          // End
        case SWT.ARROW_LEFT:   // Left arrow
        case SWT.ARROW_RIGHT:  // Right arrow
        case SWT.TAB:
        case SWT.CR:
        case SWT.KEYPAD_CR:
        case SWT.KEYPAD_DECIMAL:
            return;
    }
		if(arg0.keyCode==46)
		{
			return;
		}
    if (!Character.isDigit(arg0.character)) {
        arg0.doit = false;  // disallow the action
    }

	}
});
txtDtYOfFcraRegiNo.addVerifyListener(new VerifyListener() {
	
	@Override
	public void verifyText(VerifyEvent arg0) {
		// TODO Auto-generated method stub
//		/*if(verifyFlag== false)
//		{
//			arg0.doit= true;
//			return;
//		}*/
		switch (arg0.keyCode) {
        case SWT.BS:           // Backspace
        case SWT.DEL:          // Delete
        case SWT.HOME:         // Home
        case SWT.END:          // End
        case SWT.ARROW_LEFT:   // Left arrow
        case SWT.ARROW_RIGHT:  // Right arrow
        case SWT.TAB:
        case SWT.CR:
        case SWT.KEYPAD_CR:
        case SWT.KEYPAD_DECIMAL:
            return;
    }
		if(arg0.keyCode==46)
		{
			return;
		}
    if (!Character.isDigit(arg0.character)) {
        arg0.doit = false;  // disallow the action
    }

	}
});

		
		txtDtDOfFcraRegiNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
				{
					if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
					{
						txtDtDOfFcraRegiNo.setText("0"+ txtDtDOfFcraRegiNo.getText());
						//txtFromDtMonth.setFocus();
						txtDtMOfFcraRegiNo.setFocus();
						return;
					}
					else
					{
						txtDtMOfFcraRegiNo.setFocus();
					}
				}
				if(arg0.keyCode ==SWT.TAB)
				{
					if(txtDtDOfFcraRegiNo.getText().equals(""))
					{
						txtDtDOfFcraRegiNo.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOfFcraRegiNo.setFocus();
							}
						});
						return;
					}
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtFcraRegiNo.setFocus();
				}
			}
			
		});
		
		txtDtMOfFcraRegiNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
				{
					if(!txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
					{
						txtDtMOfFcraRegiNo.setText("0"+ txtDtMOfFcraRegiNo.getText());
						//txtFromDtMonth.setFocus();
						txtDtYOfFcraRegiNo.setFocus();
						return;
					}
					else
					{
						txtDtYOfFcraRegiNo.setFocus();
					}
				}
				if(arg0.keyCode ==SWT.TAB)
				{
					if(txtDtMOfFcraRegiNo.getText().equals(""))
					{
						txtDtMOfFcraRegiNo.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOfFcraRegiNo.setFocus();
							}
						});
						return;
					}
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtDtDOfFcraRegiNo.setFocus();
				}
			}
		});
		
		txtDtYOfFcraRegiNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR)
				{
					if(!txtDtYOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtYOfFcraRegiNo.getText())<10 && txtDtYOfFcraRegiNo.getText().length()< txtDtYOfFcraRegiNo.getTextLimit())
					{
						txtDtYOfFcraRegiNo.setText("0"+ txtDtYOfFcraRegiNo.getText());
						//txtFromDtMonth.setFocus();
						txtAddress.setFocus();
						return;
					}
					else
					{
						txtAddress.setFocus();
					}
					
					/*if((!txtDtDOfFcraRegiNo.getText().equals("") || !txtDtMOfFcraRegiNo.getText().equals("")) && txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
						if(txtDtDOfFcraRegiNo.getText().equals("") && !txtDtMOfFcraRegiNo.getText().equals("") && txtDtYOfFcraRegiNo.getText().trim().equals(""))
						{
							txtDtDOfFcraRegiNo.setFocus();
							return;
						}
						if(!txtDtDOfFcraRegiNo.getText().equals("") && txtDtMOfFcraRegiNo.getText().equals("") && txtDtYOfFcraRegiNo.getText().trim().equals(""))
						{
							txtDtDOfFcraRegiNo.setFocus();
							return;
						}
						else
						{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								
							}
						});
						return;
						}}*/
					
				}
				if(arg0.keyCode ==SWT.TAB)
				{
					if(txtDtYOfFcraRegiNo.getText().equals(""))
					{
						txtDtYOfFcraRegiNo.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
							}
						});
						return;
					}
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtDtMOfFcraRegiNo.setFocus();
				}
			}
		});
		
		txtEmailId.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode == SWT.CR)
				{
					txtTeliphoneNo.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtvatNo.setFocus();
				}
			}
		});
		 
		txtTeliphoneNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode == SWT.CR)
				{
					txtFaxNo.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtEmailId.setFocus();
				}
			}
		});
		
		txtFaxNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode == SWT.CR)
				{
					txtWebsite.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtTeliphoneNo.setFocus();
				}
			}
			
		});
		
		txtWebsite.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode == SWT.CR)
				{
					txtPAN.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtFaxNo.setFocus();
				}
			}
		});
		
		txtPAN.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode == SWT.CR)
				{
					txtServiceTaxNo.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtWebsite.setFocus();
				}
			}
		});
		
		txtServiceTaxNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode == SWT.CR)
				{
					btnSave.setFocus();
				}
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtPAN.setFocus();
				}
			}
		});
		
		btnSave.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode == SWT.ARROW_UP)
				{
					txtServiceTaxNo.setFocus();
				}
			}
		});
				
		dropdownState.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchTexttimeout = now + 1000;					
				for(int i = 0; i < dropdownState.getItemCount(); i++ )
				{
					if(dropdownState.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropdownState.select(i);
						dropdownState.notifyListeners(SWT.Selection ,new Event()  );
						break;
					}
				}
			}
		});
		
		dropdownCity.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchTexttimeout = now + 1000;					
				for(int i = 0; i < dropdownCity.getItemCount(); i++ )
				{
					if(dropdownCity.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropdownCity.select(i);
						dropdownCity.notifyListeners(SWT.Selection ,new Event()  );
						break;
					}
				}
			}
		});
		
		dropdownCountry.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchTexttimeout = now + 500;					
				for(int i = 0; i < dropdownCountry.getItemCount(); i++ )
				{
					if(dropdownCountry.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropdownCountry.select(i);
						dropdownCountry.notifyListeners(SWT.Selection ,new Event()  );
						break;
					}
				}
			}
		});
		

		
		
		txtDtDOrg.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtDtMOrg.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtDtYOrg.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtDtDOfFcraRegiNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtDtMOfFcraRegiNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtDtYOfFcraRegiNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtFaxNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if((arg0.keyCode>= 48 && arg0.keyCode <= 57) ||  arg0.keyCode== 8 || arg0.keyCode == 13||arg0.keyCode == SWT.KEYPAD_0||
						arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
						arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9)
				{
					arg0.doit = true;
				}
				else
				{
					
					arg0.doit = false;
				}
			}
		});
		
		txtDtDOrg.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				
			/*	if(!txtDtDOrg.getText().equals("") && (Integer.valueOf(txtDtDOrg.getText())> 31 || Integer.valueOf(txtDtDOrg.getText()) <= 0) )
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgdateErr.setText("Error!!");
					msgdateErr.setMessage("You have entered an Invalid Date");
					msgdateErr.open();
					
					//txtDtDOrg.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtDOrg.setFocus();
						}
					});
					return;
				}*/
				if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtDOrg.getTextLimit())
				{
					txtDtDOrg.setText("0"+ txtDtDOrg.getText());
					//txtFromDtMonth.setFocus();
					return;
				}
			}
		});
		
		txtDtMOrg.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
			
				/*if(!txtDtMOrg.getText().equals("") && (Integer.valueOf(txtDtMOrg.getText())> 12 || Integer.valueOf(txtDtMOrg.getText()) <= 0) )
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgdateErr.setText("Error!");
					msgdateErr.setMessage("You have entered an Invalid Month.");
					msgdateErr.open();
					
					
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							//txtDtMOrg.setText("");
							txtDtMOrg.setFocus();
							txtDtMOrg.selectAll();
							
						}
					});
					return;
				}*/
				if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtDOrg.getTextLimit())
				{
					txtDtMOrg.setText("0"+ txtDtMOrg.getText());
					//txtFromDtMonth.setFocus();
					return;
				}
			}
		});
				
		txtDtYOrg.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				
			/*	if(!txtDtYOrg.getText().trim().equals("")&& Integer.valueOf(txtDtYOrg.getText())<1990 )
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgDayErr.setText("Error!");
					msgDayErr.setMessage("You have entered an Invalid Year.");
				    msgDayErr.open();
					//txtDtYOrg.setText("");
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtYOrg.setFocus();
							txtDtYOrg.selectAll();
							
						}
					});
					return;
				}*/
				
			/*	if((txtDtDOrg.getText().trim().equals("") && ! txtDtMOrg.getText().equals("") && ! txtDtYOrg.getText().equals("")) || ((txtDtDOrg.getText().trim().equals("") && (! txtDtMOrg.getText().equals("") || ! txtDtYOrg.getText().equals("")))))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgDayErr.setText("Validation Date Error!");
					msgDayErr.setMessage("Please enter a date in DD format.");
					msgDayErr.open();
					
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtDOrg.setFocus();
							
						}
					});
					return;
				}
				
				if((txtDtDOrg.getText().equals("") && txtDtMOrg.getText().trim().equals("") && !txtDtYOrg.getText().equals(""))||((!txtDtDOrg.getText().equals("") || !txtDtYOrg.getText().equals("")) && txtDtMOrg.getText().trim().equals("")))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgDayErr.setText("Validation Date Error!");
					msgDayErr.setMessage("Please enter a Month in MM format.");
					msgDayErr.open();
					
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtMOrg.setFocus();
							
						}
					});
					return;
				}*/

				/*if((!txtDtDOrg.getText().equals("") || !txtDtMOrg.getText().equals("")) && txtDtYOrg.getText().trim().equals(""))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgDayErr.setText("Validation Date Error!");
					msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
					msgDayErr.open();
					
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtYOrg.setFocus();
							
						}
					});
					return;
				}*/

				
		/*		if(txtDtYOrg.getText().length( ) < txtDtYOrg.getTextLimit())
				{
					MessageBox msgYearErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msgYearErr.setMessage("Please enter year in 4 digits format (YYYY)");
					msgYearErr.open();
					txtDtYOrg.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtYOrg.setFocus();
							txtDtYOrg.selectAll();
							
						}
					});
					return;
					
				}*/
				/*if((txtDtDOrg.getText().equals("") && txtDtMOrg.getText().equals("")&& txtDtYOrg.getText().equals("")) || (!(txtDtDOrg.getText().equals("")) && !(txtDtMOrg.getText().equals("")) && !(txtDtYOrg.getText().equals(""))))
				{
					if(!txtDtDOrg.getText().equals("") && (Integer.valueOf(txtDtDOrg.getText())> 31 || Integer.valueOf(txtDtDOrg.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid date");
						msgdateErr.open();
						
						txtDtDOrg.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOrg.setFocus();
								return;
							}
						});
						return;
					}
					
					if(!txtDtDOrg.getText().equals("") && Integer.valueOf ( txtDtDOrg.getText())<10 && txtDtDOrg.getText().length()< txtDtMOrg.getTextLimit())
					{	
						String dt = txtDtDOrg.getText();
						txtDtDOrg.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						
						//txtDtYOrg.setFocus();
						return;
					}	
					
					if(!txtDtMOrg.getText().equals("") && (Integer.valueOf(txtDtMOrg.getText())> 12 || Integer.valueOf(txtDtMOrg.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid Month");
						msgdateErr.open();
						
						txtDtMOrg.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOrg.setFocus();
								return;
							}
							
						});
						return;
					}
					
					if(!txtDtMOrg.getText().equals("") && Integer.valueOf ( txtDtMOrg.getText())<10 && txtDtMOrg.getText().length()< txtDtMOrg.getTextLimit())
					{	
						String dt = txtDtMOrg.getText();
						txtDtMOrg.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						
						//txtDtYOrg.setFocus();
						return;
					}}	
				else
				{
					if(txtDtDOrg.getText().trim().equals("") )
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a date in DD format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOrg.setFocus();
								return;	
							}
						});
						return;
					}
					
					
					if(txtDtMOrg.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Month in MM format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOrg.setFocus();
								return;
							}
						});
						return;
					}
					
				
					if(txtDtYOrg.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOrg.setFocus();
								return;
							}
							
						});
						return;
					}
				}*/
		
				if(!txtDtYOrg.getText().equals("")&&Integer.valueOf(txtDtYOrg.getText()) > 1900)
				{
			/*	DateValidate dv = new DateValidate(Integer.valueOf(txtDtDOrg.getText()) ,Integer.valueOf(txtDtMOrg.getText()) ,Integer.valueOf(txtDtYOrg.getText()));
				String validationResult = dv.toString();
				if(validationResult.substring(2,3).equals("0"))
				{
					MessageBox msgErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msgErr.setMessage("You have entered invalid Date");
					msgErr.open();
				
					Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtDtDOrg.setFocus();
						
					}
				});
				return;
				}*/
				Calendar cal = Calendar.getInstance();
				try 
				{
					cal.set(Integer.valueOf(Integer.valueOf(txtDtYOrg.getText())-1),( Integer.valueOf(txtDtMOrg.getText())-1 )  , (Integer.valueOf(txtDtDOrg.getText())-1) );
				} catch (NumberFormatException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				cal.add(Calendar.YEAR , 1);
				Date nextYear = cal.getTime();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String FinalDate = sdf.format(nextYear);
				
				/*txtToDtDay.setText(FinalDate.substring(0,2) );
				txtToDtMonth.setText(FinalDate.substring(3,5));
				txtToDtYear.setText(FinalDate.substring(6));*/
				}
			}
			
			
		});
		
		
		txtDtDOfFcraRegiNo.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
			
			/*	if(!txtDtDOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtDOfFcraRegiNo.getText())> 31 || Integer.valueOf(txtDtDOfFcraRegiNo.getText()) <= 0) )
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgdateErr.setText("Error!");
					msgdateErr.setMessage("You have entered an Invalid Date.");
					msgdateErr.open();
					
					txtDtDOfFcraRegiNo.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						public void run() {
							// TODO Auto-generated method stub
							txtDtDOfFcraRegiNo.setFocus();
							
						}
					});
					return;
				}*/
				if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
				{
					txtDtDOfFcraRegiNo.setText("0"+ txtDtDOrg.getText());
					//txtFromDtMonth.setFocus();
					return;
				}
				}
		});
		
		txtDtMOfFcraRegiNo.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
			
			/*	if(!txtDtMOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtMOfFcraRegiNo.getText())> 12 || Integer.valueOf(txtDtMOfFcraRegiNo.getText()) <= 0))
				{
					MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgdateErr.setText("Validation Error!");
					msgdateErr.setMessage("You have entered an Invalid Month.");
					msgdateErr.open();
					
					
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							//txtDtMOfFcraRegiNo.setText("");
							txtDtMOfFcraRegiNo.setFocus();
							txtDtMOfFcraRegiNo.selectAll();
							
						}
					});
					return;
					
				}*/
				if(! txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
				{
					txtDtMOfFcraRegiNo.setText("0"+ txtDtMOfFcraRegiNo.getText());
					return;
				}
				}
		});
		
		txtDtYOfFcraRegiNo.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
			
				if(!txtDtYOfFcraRegiNo.getText().trim().equals("")&& Integer.valueOf(txtDtYOfFcraRegiNo.getText())<1990)
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgDayErr.setText("Validation Error!");
					msgDayErr.setMessage("You have entered an Invalid Year.");
					msgDayErr.open();
				//	txtDtYOfFcraRegiNo.setText("");
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtYOfFcraRegiNo.setFocus();
							txtDtYOfFcraRegiNo.selectAll();
							
						}
					});
					return;
				}
				
				/*if((!txtDtDOfFcraRegiNo.getText().equals("") || !txtDtMOfFcraRegiNo.getText().equals("")) && txtDtYOfFcraRegiNo.getText().trim().equals(""))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgDayErr.setText("Validation Date Error!");
					msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
					msgDayErr.open();
					
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtYOfFcraRegiNo.setFocus();
							return;
						}
					});
					return;
				}*/
			/*	if((txtDtDOfFcraRegiNo.getText().trim().equals("") && ! txtDtMOfFcraRegiNo.getText().equals("") && ! txtDtYOfFcraRegiNo.getText().equals("")) || ((txtDtDOfFcraRegiNo.getText().trim().equals("") && (! txtDtMOfFcraRegiNo.getText().equals("") || ! txtDtYOfFcraRegiNo.getText().equals("")))))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgDayErr.setText("Validation Date Error!");
					msgDayErr.setMessage("Please enter a date in DD format.");
					msgDayErr.open();
					
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtDOfFcraRegiNo.setFocus();
							return;
						}
					});
					return;
				}
				
				if((!txtDtDOfFcraRegiNo.getText().equals("") && txtDtMOfFcraRegiNo.getText().trim().equals("") && !txtDtYOfFcraRegiNo.getText().equals(""))||((!txtDtDOfFcraRegiNo.getText().equals("") || !txtDtYOfFcraRegiNo.getText().equals("")) && txtDtMOfFcraRegiNo.getText().trim().equals("")))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgDayErr.setText("Validation Date Error!");
					msgDayErr.setMessage("Please enter a Month in MM format.");
					msgDayErr.open();
					
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtMOfFcraRegiNo.setFocus();
							return;
						}
					});
					return;
				}*/

				/*if((!txtDtDOfFcraRegiNo.getText().equals("") || !txtDtMOfFcraRegiNo.getText().equals("")) && txtDtYOfFcraRegiNo.getText().trim().equals(""))
				{
					MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msgDayErr.setText("Validation Date Error!");
					msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
					msgDayErr.open();
					
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtYOfFcraRegiNo.setFocus();
							return;
						}
					});
					return;
				}*/
				
				
				
			
				
				/*if((txtDtDOfFcraRegiNo.getText().equals("") && txtDtMOfFcraRegiNo.getText().equals("")&& txtDtYOfFcraRegiNo.getText().equals("")) || (!(txtDtDOfFcraRegiNo.getText().equals("")) && !(txtDtMOfFcraRegiNo.getText().equals("")) && !(txtDtYOfFcraRegiNo.getText().equals(""))))
				{
					if(!txtDtDOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtDOfFcraRegiNo.getText())> 31 || Integer.valueOf(txtDtDOfFcraRegiNo.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid date");
						msgdateErr.open();
						
						txtDtDOfFcraRegiNo.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOfFcraRegiNo.setFocus();
								return;
							}
						});
						return;
					}
			
					if(!txtDtDOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtDOfFcraRegiNo.getText())<10 && txtDtDOfFcraRegiNo.getText().length()< txtDtDOfFcraRegiNo.getTextLimit())
					{	
						String dt = txtDtDOfFcraRegiNo.getText();
						txtDtDOfFcraRegiNo.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						
						//txtDtYOrg.setFocus();
						return;
					}	
					
					
					if(!txtDtMOfFcraRegiNo.getText().equals("") && (Integer.valueOf(txtDtMOfFcraRegiNo.getText())> 12 || Integer.valueOf(txtDtMOfFcraRegiNo.getText()) <= 0) )
					{
						MessageBox msgdateErr = new MessageBox(new Shell(), SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgdateErr.setText("Validation Date Error!");
						msgdateErr.setMessage("You have entered an invalid Month");
						msgdateErr.open();
						
						txtDtMOfFcraRegiNo.setText("");
						Display.getCurrent().asyncExec(new Runnable() {
							
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOfFcraRegiNo.setFocus();
								return;
							}
							
						});
						return;
					}
					if(!txtDtMOfFcraRegiNo.getText().equals("") && Integer.valueOf ( txtDtMOfFcraRegiNo.getText())<10 && txtDtMOfFcraRegiNo.getText().length()< txtDtMOfFcraRegiNo.getTextLimit())
					{	
						String dt = txtDtMOfFcraRegiNo.getText();
						txtDtMOfFcraRegiNo.setText("0"+dt);
						//txtFromDtMonth.setFocus();
						
						//txtDtYOrg.setFocus();
						return;
					}	
					
	}	*/
				/*else
				{
					if(txtDtDOfFcraRegiNo.getText().trim().equals("") )
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a date in DD format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtDOfFcraRegiNo.setFocus();
								return;
							}
						});
						return;
					}
					
					
					if(txtDtMOfFcraRegiNo.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Month in MM format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtMOfFcraRegiNo.setFocus();
								return;
							}
						});
						return;
					}
					
				
					if(!txtDtDOfFcraRegiNo.getText().equals("") && !txtDtMOfFcraRegiNo.getText().equals("") && txtDtYOfFcraRegiNo.getText().trim().equals(""))
					{
						MessageBox msgDayErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
						msgDayErr.setText("Validation Date Error!");
						msgDayErr.setMessage("Please enter a Year in yyyy in valid format.");
						msgDayErr.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtDtYOfFcraRegiNo.setFocus();
								return;
							}
						});
						return;
					}
			
				}*/
				/*if(txtDtYOfFcraRegiNo.getText().length( ) < txtDtYOfFcraRegiNo.getTextLimit())
				{
					MessageBox YearErr = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					YearErr.setMessage("Please enter year in 4 digits format (YYYY)");
					YearErr.open();
					txtDtYOfFcraRegiNo.setText("");
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtDtYOfFcraRegiNo.setFocus();
							txtDtYOfFcraRegiNo.selectAll();
							
						}
					});
					return;
					
				}*/
				if(!txtDtYOfFcraRegiNo.getText().equals("")&&Integer.valueOf(txtDtYOfFcraRegiNo.getText()) > 1900)
				{
				/*DateValidate dv = new DateValidate(Integer.valueOf(txtDtDOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtMOfFcraRegiNo.getText()) ,Integer.valueOf(txtDtYOfFcraRegiNo.getText()));
				String validationResult = dv.toString();
				if(validationResult.substring(2,3).equals("0"))
				{
					MessageBox Err = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					Err.setMessage("You have entered invalid Date");
					Err.open();
				
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtDtDOfFcraRegiNo.setFocus();
						
					}
				});
				return;
				}*/
				Calendar cal = Calendar.getInstance();
				try 
				{
					cal.set(Integer.valueOf(Integer.valueOf(txtDtYOfFcraRegiNo.getText())-1),( Integer.valueOf(txtDtMOfFcraRegiNo.getText())-1 )  , (Integer.valueOf(txtDtDOfFcraRegiNo.getText())-1) );
				} catch (NumberFormatException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				cal.add(Calendar.YEAR , 1);
				Date nextYear = cal.getTime();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String FinalDate = sdf.format(nextYear);
				
				/*txtToDtDay.setText(FinalDate.substring(0,2) );
				txtToDtMonth.setText(FinalDate.substring(3,5));
				txtToDtYear.setText(FinalDate.substring(6));*/
				
				}
			}
			
		});
	}
	
	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}


	
	protected void checkSubclass()
	{
		//this is blank method so will disable the check that prevents subclassing of shells.
	}


}
