package gnukhata.views;

import gnukhata.controllers.accountController;
import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.transactionController;

import java.awt.Checkbox;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.xmlrpc.XmlRpcException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.jopendocument.sample.SpreadSheetCreation;

import sun.org.mozilla.javascript.regexp.SubString;
import sun.security.util.Length;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;
/*import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
*/
import com.sun.org.apache.bcel.internal.generic.NEW;

public class LedgerRecon extends Composite {

	String strOrgName;
	String strFromYear;
	String strToYear;
	String accname;
	int counter=0;
	boolean narration1;

	Table ledgerReconReport;
	TableColumn Srno;
	TableColumn Date;
	TableColumn Particulars;
	TableColumn VoucherNumber;
	TableColumn Dr;
	TableColumn Cr;
	TableColumn Narration;
	TableColumn Clrdate;
	TableColumn Memo;
	TableItem headerRow;
	Label lblOrgDetails;
	Label lblheadline;
	Label lblorgname;
	Label lblsrno;
	Label lblDate;
	Label lblParticulars;
	Label lblVoucherNumber;
	Label lblNarration;
	Label lblDr;
	Label lblCr;
	Label lblclrdate;
	Label lblmemo;
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	Label lblPageName;
	Label lblPeriod;
	TableEditor srnoeditor;
	TableEditor DateEditor;
	TableEditor ParticularEditor;
	TableEditor VoucherNumberEditor;
	TableEditor DrEditotr;
	TableEditor CrEditor;
	TableEditor clreditor;
	TableEditor memoeditor;
	
	TableEditor NarrationEditor;
	static Display display;
	
	Button btnbacktoviewrecon;
	//Button btnViewLedgerReconLedgerForAccount;
	Button btnRecon;
	//Button btnViewLedgerReconDualLedger;
	Label cleareditem;
	//Button Btnclear;
	//String tb;
	String strFromDate;
	String strToDate;

	
	
	String bankname="";
	String projectName="";
	String ledgerProject;
	//boolean narration1;
	boolean projectflag;
	//boolean clearedFlag;
	Button btnviewclrunclr;
	String ToDate;
	String FromDate;
	
	Text txtcldt;
	Text txtmem;
	
	
	ArrayList<Text> txtcleardates = new ArrayList<Text>();
	ArrayList<Text>  txtmemos= new ArrayList<Text>();
	ArrayList<String> voucherCodes = new ArrayList<String>();
	ArrayList<String> refdates=new ArrayList<String>();
	ArrayList<String> accountnames=new ArrayList<String>();
	ArrayList<String> dramounts=new ArrayList<String>();
	ArrayList<String> cramounts=new ArrayList<String>();
	//ArrayList<String> narrations=new ArrayList<String>();
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	//Vector<Object> printLedgerData = new Vector<Object>();
	public LedgerRecon(Composite parent,int style,String selectbank,String fromdate,String toDate,boolean narrationFlag,String ProjectName, Object[] result) {
		super(parent, style);
		// TODO Auto-generated constructor stub
		sdf.setLenient(false);
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		
		ToDate = toDate;
		FromDate = fromdate;
		narration1= narrationFlag;
		//clearedFlag = Btnclear.getSelection();
		bankname=selectbank;
		projectName=ProjectName;
	
		//old values
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout =new FormData();
	    
	    MainShell.lblLogo.setVisible(false);
		MainShell.lblLine.setVisible(false);
		MainShell.lblOrgDetails.setVisible(false);
	    
	    strFromDate=fromdate.substring(8)+"-"+fromdate.substring(5,7)+"-"+fromdate.substring(0,4);
		strToDate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0,4);
		
		/*Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();

		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(70);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);*/
		
		/*Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 14, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 26, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(98);
		layout.bottom = new FormAttachment(14);
		lblLine.setLayoutData(layout);
		
*/		
		Label lblbankLabel=new Label(this, SWT.NONE);
		lblbankLabel.setText("Bank Reconciliation");
		lblbankLabel.setFont(new Font(display, "Times New Roman", 15, SWT.NORMAL| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(38);
		lblbankLabel.setLayoutData(layout);
		
		
		lblheadline=new Label(this, SWT.NONE);
		lblheadline.setText(""+globals.session[1]);
		lblheadline.setFont(new Font(display, "Times New Roman", 11, SWT.NORMAL| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblbankLabel,2);
		layout.left = new FormAttachment(42);
		lblheadline.setLayoutData(layout);
	

		
		Label lblAccName=new Label(this, SWT.NONE);
		lblAccName.setFont( new Font(display,"Times New Roman", 11, SWT.NORMAL | SWT.BOLD) );
		lblAccName.setText("Bank Name: "+bankname);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(5);
		lblAccName.setLayoutData(layout);
		

		lblPageName = new Label(this, SWT.NONE);
		lblPageName.setFont(new Font(display, "Times New Roman", 11, SWT.NORMAL | SWT.BOLD));
		lblPageName.setText("Period From "+strFromDate+" To "+ strToDate);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,1);
		layout.left = new FormAttachment(60);
		lblPageName.setLayoutData(layout);
		
		/*cleareditem  = new Label(this, SWT.NONE);
		//lblPageName.setText("Ledger for account : "+ accountName );
		cleareditem.setFont(new Font(display, "Times New Roman", 11, SWT.NORMAL | SWT.BOLD));
		cleareditem.setText("Viewing Cleared-Uncleared Items");
		layout = new FormData();
		layout.top = new FormAttachment(lblPageName,1);
		layout.left = new FormAttachment(30);
		cleareditem.setLayoutData(layout);*/
	
		/*Btnclear=new Button(this, SWT.CHECK);
		Btnclear.setText("Cleared Transactions");
		layout = new FormData();
		layout.top=new FormAttachment(cleareditem, 1);
		layout.left = new FormAttachment(36);
		Btnclear.setLayoutData(layout);
		Btnclear.setVisible(true);*/
		
		btnviewclrunclr =new Button(this,SWT.PUSH);
		btnviewclrunclr.setText("View Cleared and Uncleared Items");
		btnviewclrunclr.setFont(new Font(display,"Times New Roman",11,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(lblPageName,1);
		layout.left=new FormAttachment(35);
		btnviewclrunclr.setLayoutData(layout);
		
		
	    ledgerReconReport= new Table(this, SWT.MULTI|SWT.BORDER|SWT.FULL_SELECTION|SWT.LINE_SOLID);
	    ledgerReconReport.setLinesVisible (true);
		ledgerReconReport.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(btnviewclrunclr,10);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(95);
		layout.bottom = new FormAttachment(82);
		ledgerReconReport.setLayoutData(layout);
				
		btnRecon =new Button(this,SWT.PUSH);
		btnRecon.setText(" R&econcile ");
		btnRecon.setFont(new Font(display,"Times New Roman",11,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(83);
		layout.left=new FormAttachment(36);
		layout.bottom= new FormAttachment(87);
		btnRecon.setLayoutData(layout);
		btnRecon.setEnabled(false);
		
		btnbacktoviewrecon=new Button(this, SWT.PUSH);
		btnbacktoviewrecon.setText(" &Back To View Reconciliation");
		btnbacktoviewrecon.setFont(new Font(display,"Times New Roman",11,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(83);
		layout.left=new FormAttachment(45);
		layout.bottom= new FormAttachment(87);
		btnbacktoviewrecon.setLayoutData(layout);
		
		

				
	  
	    this.makeaccssible(ledgerReconReport);
	    this.getAccessible();
	    this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
	    this.setReport(result);
	    //this.pack();
	    this.setEvents();
	   

	}
	public void makeaccssible(Control c)
	{
		c.getAccessible();
	}
	private void setReport(Object[]result)
	{
		
		ledgerReconReport.setFocus();
		
		lblDate = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblDate.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		lblDate.setText("    Date   ");
		
		lblParticulars = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblParticulars.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblParticulars = new Label(ledgerReconReport,SWT.BORDER);
		lblParticulars.setText("    		Particulars		     ");
		
		lblVoucherNumber = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblVoucherNumber.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblVoucherNumber = new Label(ledgerReconReport,SWT.BORDER);
		lblVoucherNumber.setText(" V.No");
		
		lblDr = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblDr.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblDr = new Label(ledgerReconReport,SWT.BORDER);
		lblDr.setText(" Debit ");
		
		lblCr = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblCr.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblCr = new Label(ledgerReconReport,SWT.BORDER);
		lblCr.setText(" Credit ");
		
		lblNarration = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblNarration.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblNarration.setText("   Narration   ");
		
		lblclrdate = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblclrdate.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblclrdate.setText("Clearance Date");
		
		lblmemo = new Label(ledgerReconReport,SWT.BORDER|SWT.CENTER);
		lblmemo.setFont(new Font(display, "Times New Roman", 11, SWT.BOLD));
		//lblNarration = new Label(ledgerReconReport,SWT.BORDER);
		lblmemo.setText("   Memo   ");
		
		  final int ledgwidth = ledgerReconReport.getClientArea().width;
		  
		//lblclrdate
		 
	    ledgerReconReport.addListener(SWT.MeasureItem, new Listener() 
		{
	    	@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				// Srno.setWidth(5 * ledgwidth / 100);
	    		if(narration1==true)
				{
				
					Date.setWidth(6 * ledgwidth / 100);
					Particulars.setWidth(26 * ledgwidth / 100);
					VoucherNumber.setWidth(6 * ledgwidth / 100);
					Dr.setWidth(8 * ledgwidth / 100);
					Cr.setWidth(8 * ledgwidth / 100);
					Narration.setWidth(24 * ledgwidth / 100);
					Clrdate.setWidth(6 * ledgwidth / 100);
					Memo.setWidth(16 * ledgwidth / 100);
				}
	    		else
				{
					Date.setWidth(10 * ledgwidth / 100);
					Particulars.setWidth(25 * ledgwidth / 100);
					VoucherNumber.setWidth(10 * ledgwidth / 100);
					Dr.setWidth(15 * ledgwidth / 100);
					Cr.setWidth(15 * ledgwidth / 100);
					Clrdate.setWidth(10 * ledgwidth / 100);
					Memo.setWidth(15 * ledgwidth / 100);
		
				}


								
				event.height = 11;
			    
			    
			};
		});
		
	    headerRow = new TableItem(ledgerReconReport, SWT.NONE);
		TableItem[] items = ledgerReconReport.getItems();
		//Srno = new TableColumn(ledgerReconReport, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
		Date = new TableColumn(ledgerReconReport, SWT.BORDER|SWT.BACKGROUND| SWT.CENTER );
		Particulars = new TableColumn(ledgerReconReport, SWT.BORDER);
		VoucherNumber = new TableColumn(ledgerReconReport, SWT.RIGHT);
		Dr= new TableColumn(ledgerReconReport, SWT.RIGHT);
		Cr= new TableColumn(ledgerReconReport, SWT.RIGHT);
		if(narration1==true)
		{
			Narration=new TableColumn(ledgerReconReport, SWT.BORDER);
		}
		Clrdate= new TableColumn(ledgerReconReport, SWT.RIGHT);
		Memo= new TableColumn(ledgerReconReport, SWT.RIGHT);

		
			
		if(narration1==true)
		{
		
	    DateEditor = new TableEditor(ledgerReconReport);
		DateEditor.grabHorizontal = true;
		DateEditor.setEditor(lblDate,items[0],0);
		
		ParticularEditor = new TableEditor(ledgerReconReport);
		ParticularEditor.grabHorizontal = true;
		ParticularEditor.setEditor(lblParticulars,items[0],1);
		
		VoucherNumberEditor = new TableEditor(ledgerReconReport);
		VoucherNumberEditor.grabHorizontal = true;
		VoucherNumberEditor.setEditor(lblVoucherNumber,items[0],2);
		
		DrEditotr = new TableEditor(ledgerReconReport);
		DrEditotr.grabHorizontal = true;
		DrEditotr.setEditor(lblDr,items[0],3);
		
		CrEditor = new TableEditor(ledgerReconReport);
		CrEditor.grabHorizontal = true;
		CrEditor.setEditor(lblCr,items[0],4);
		
		NarrationEditor = new TableEditor(ledgerReconReport);
		NarrationEditor.grabHorizontal = true;
		NarrationEditor.setEditor(lblNarration,items[0],5);
		
		
		clreditor = new TableEditor(ledgerReconReport);
		clreditor.grabHorizontal = true;
		clreditor.setEditor(lblclrdate,items[0],6);
		memoeditor = new TableEditor(ledgerReconReport);
		memoeditor.grabHorizontal = true;
		memoeditor.setEditor(lblmemo,items[0],7);

		//Srno.pack();
		Date.pack();
		Particulars.pack();
		VoucherNumber.pack();
	    Dr.pack();
		Cr.pack();
		Narration.pack();
		Clrdate.pack();
		Memo.pack();
		}
		else
		{
			

		    DateEditor = new TableEditor(ledgerReconReport);
			DateEditor.grabHorizontal = true;
			DateEditor.setEditor(lblDate,items[0],0);
			
			ParticularEditor = new TableEditor(ledgerReconReport);
			ParticularEditor.grabHorizontal = true;
			ParticularEditor.setEditor(lblParticulars,items[0],1);
			
			VoucherNumberEditor = new TableEditor(ledgerReconReport);
			VoucherNumberEditor.grabHorizontal = true;
			VoucherNumberEditor.setEditor(lblVoucherNumber,items[0],2);
			
			DrEditotr = new TableEditor(ledgerReconReport);
			DrEditotr.grabHorizontal = true;
			DrEditotr.setEditor(lblDr,items[0],3);
			
			CrEditor = new TableEditor(ledgerReconReport);
			CrEditor.grabHorizontal = true;
			CrEditor.setEditor(lblCr,items[0],4);
			
			
			clreditor = new TableEditor(ledgerReconReport);
			clreditor.grabHorizontal = true;
			clreditor.setEditor(lblclrdate,items[0],5);
			
			memoeditor = new TableEditor(ledgerReconReport);
			memoeditor.grabHorizontal = true;
			memoeditor.setEditor(lblmemo,items[0],6);

			//Srno.pack();
			Date.pack();
			Particulars.pack();
			VoucherNumber.pack();
		    Dr.pack();
			Cr.pack();
			
			//Narration.pack();
			Clrdate.pack();
			Memo.pack();
		}
		
		for(int rowcounter =0; rowcounter < result.length; rowcounter ++)
		{
			
			TableItem tbrow = new TableItem(ledgerReconReport , SWT.NONE);
			Object[] ledgerrecorecord = (Object[]) result[rowcounter];
			
			tbrow.setText(0, ledgerrecorecord[0].toString());
			refdates.add(ledgerrecorecord[0].toString());
			tbrow.setText(1,ledgerrecorecord[1].toString() );
			if(!ledgerrecorecord[2].toString().equals(""))
				{
				accountnames.add(ledgerrecorecord[1].toString() );
				}
			
			
			tbrow.setText(2, ledgerrecorecord[2].toString());
			if(rowcounter==result.length-8 || rowcounter==result.length-7 || (rowcounter == 0 && ledgerrecorecord[2].toString().equals("")))
			{
				System.out.println("inside if condition");
				tbrow.setText(3, ledgerrecorecord[3].toString());
				tbrow.setText(4, ledgerrecorecord[4].toString());
			}
			else
			{
				voucherCodes.add(ledgerrecorecord[3].toString());
				tbrow.setText(3, ledgerrecorecord[4].toString());
				dramounts.add(ledgerrecorecord[4].toString());
				tbrow.setText(4, ledgerrecorecord[5].toString());
				cramounts.add(ledgerrecorecord[5].toString());
			}
			if(narration1==true)
			{
					try {
						tbrow.setText(5,ledgerrecorecord[6].toString() );
					} catch (Exception e) {
						// TODO Auto-generated catch block
						tbrow.setText(5,"" );
					}
					if(!(ledgerrecorecord[2].equals("") || ledgerrecorecord[2].equals("Total")))
					{
						txtcldt = new Text(ledgerReconReport , SWT.NONE);
						txtcldt.setVisible(true);
						txtcldt.setText(ledgerrecorecord[7].toString());
						txtcldt.setMessage("DD-MM-YYYY");
						txtcldt.setToolTipText("Enter date in DD-MM-YYYY format");
			            TableEditor txteditcl = new TableEditor(ledgerReconReport);
						txteditcl.grabHorizontal = true;
						txteditcl.setEditor(txtcldt,tbrow,6);
						txtcleardates.add(txtcldt);
						//txtcldt.setFocus();
						/*if(!txtcleardates.get(txtcleardates.size()-1 ).getText().equals(""))
						{
							txtcleardates.get(txtcleardates.size()-1 ).setEditable(true);
						}*/
						txtmem = new Text(ledgerReconReport , SWT.NONE);
						txtmem.setEditable(true);
						txtmem.setText(ledgerrecorecord[8].toString());
						TableEditor txteditmem = new TableEditor(ledgerReconReport);
						txtmem.setText("");
						txteditmem.grabHorizontal = true;
						txteditmem.setEditor(txtmem,tbrow,7);
						txtmemos.add(txtmem);
						/*if(!txtmemos.get(txtmemos.size()-1 ).getText().equals(""))
						{
							txtmemos.get(txtmemos.size()-1 ).setEditable(false);
						}*/
					}
			}
			else
			{
				if(!(ledgerrecorecord[2].equals("") || ledgerrecorecord[2].equals("Total")))
				{
					txtcldt = new Text(ledgerReconReport , SWT.NONE);
					txtcldt.setVisible(true);
					txtcldt.setText(ledgerrecorecord[7].toString());
					txtcldt.setMessage("DD-MM-YYYY");
					txtcldt.setToolTipText("Enter date in DD-MM-YYYY format");
		            TableEditor txteditcl = new TableEditor(ledgerReconReport);
					txteditcl.grabHorizontal = true;
					txteditcl.setEditor(txtcldt,tbrow,5);
					txtcleardates.add(txtcldt);
					/*if(!txtcleardates.get(txtcleardates.size()-1 ).getText().equals(""))
					{
						txtcleardates.get(txtcleardates.size()-1 ).setEditable(false);
					}*/
					//txtcldt.setFocus();
					
					txtmem = new Text(ledgerReconReport , SWT.NONE);
					txtmem.setEditable(true);
					txtmem.setText(ledgerrecorecord[8].toString());
					TableEditor txteditmem = new TableEditor(ledgerReconReport);
					txtmem.setText("");
					txteditmem.grabHorizontal = true;
					txteditmem.setEditor(txtmem,tbrow,6);
					txtmemos.add(txtmem);	
					/*if(!txtmemos.get(txtmemos.size()-1 ).getText().equals(""))
					{
						txtmemos.get(txtmemos.size()-1 ).setEditable(false);
					}*/
				}	
			}		
		}

		}
	
	
	
	public void setEvents()
	{
		
		
		//ledgerReconReport.setFocus();
		if(txtcleardates.size()==0)
		{
			btnviewclrunclr.setFocus();
			
		}
		else
		{
			System.out.println("Condition true");
			ledgerReconReport.setFocus();
		}
		/*ledgerReconReport.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				txtcleardates.get(0).setFocus();
				
				
			}
		});*/
		
		for(int clearcounter=counter; clearcounter < txtcleardates.size(); clearcounter++)
		{
			
			txtcleardates.get(clearcounter).addModifyListener(new ModifyListener() 
			{
				
				@Override
				public void modifyText(ModifyEvent arg0) {
					// TODO Auto-generated method stub
					if(!txtcleardates.get(counter).equals(""))
					{	
						btnRecon.setEnabled(true);
					}
					if(txtcleardates.get(counter).equals(""))
					{	
						btnRecon.setEnabled(false);
					}
				}
			});
			
			txtcleardates.get(clearcounter).addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if((arg0.keyCode>= 48 && arg0.keyCode <= 57) || arg0.keyCode== 45 || arg0.keyCode== 8 || arg0.keyCode == 13||
							arg0.keyCode == SWT.KEYPAD_0||arg0.keyCode == SWT.KEYPAD_1||arg0.keyCode == SWT.KEYPAD_2||arg0.keyCode == SWT.KEYPAD_3||arg0.keyCode == SWT.KEYPAD_4||
							arg0.keyCode == SWT.KEYPAD_5||arg0.keyCode == SWT.KEYPAD_6||arg0.keyCode == SWT.KEYPAD_7||arg0.keyCode == SWT.KEYPAD_8||arg0.keyCode == SWT.KEYPAD_9||arg0.keyCode == SWT.KEYPAD_CR || arg0.keyCode == SWT.KEYPAD_SUBTRACT
							||arg0.keyCode == SWT.ARROW_LEFT||arg0.keyCode == SWT.ARROW_RIGHT)
					{
						arg0.doit = true;
					}
					else
					{
						
						arg0.doit = false;
					}
					
				}
			});
			
	/*		txtcleardates.get(clearcounter).addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusLost(arg0);
					final Text curClearDate = (Text) arg0.widget;
					MessageBox msgErrorDate = new MessageBox(new Shell(), SWT.OK );
					msgErrorDate.setMessage("Please enter a valid date in DD-MM-YYYY format");
					if(curClearDate.getText().length() != 10)
					{
						msgErrorDate.open();
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								curClearDate.setFocus();
								
							}
						});
						return;
					}
					if(! curClearDate.getText().substring(2,3).equals("-") && ! curClearDate.getText().substring(5,8).equals("-") )
					{
						msgErrorDate.open();
						Display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								curClearDate.setFocus();
								
							}
						});
						return;
					}
					
				}
				
			});*/
			
			txtcleardates.get(clearcounter).addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					
					if((arg0.keyCode==SWT.CR || arg0.keyCode==SWT.KEYPAD_CR) && counter < txtcleardates.size()-1)
					{
						counter++;
						Text txtclear=(Text) arg0.widget;
						//System.out.println("hi..." +counter);
						if(counter >= 0 && counter < txtcleardates.size())
						{	
							
							String clearedDate=txtclear.getText();	
							String refdate=refdates.get(counter).toString();
							
							/*int clrday= Integer.parseInt(clearedDate.substring(0, 2));
							int clrmonth= Integer.parseInt(clearedDate.substring(3, 5));*/
							
						
							try{
								Date clrdate = sdf.parse(clearedDate);
					        	//Date todate = sdf.parse(strToDate);
					        	Date referencedate=sdf.parse(refdate);
					        	
					        	System.out.println(sdf.format(clrdate));
					        	//System.out.println(sdf.format(todate));
					        	System.out.println(sdf.format(referencedate));
					 
					        	if(clrdate.compareTo(referencedate)<0)
					        	{
					        		MessageBox msg=new MessageBox(new Shell(),SWT.OK |SWT.ICON_INFORMATION);
					        		msg.setText("Alert!");
									msg.setMessage("Clearance Date is less than transaction date");
									msg.open();
									Display.getCurrent().asyncExec(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											txtcleardates.get(counter).setFocus();
										}
									});
					        	}
					        	
					        	/*if(clrday > 31 || clrmonth > 12)
					        	{
					        		MessageBox msg=new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					        		msg.setText("Information!");
									msg.setMessage("Please enter valid date");
									msg.open();
									Display.getCurrent().asyncExec(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											txtcleardates.get(counter).setFocus();
										}
									});
					        	}*/
					        	
					    	}catch(ParseException ex){
					    		ex.printStackTrace();
					    	}
							
							txtcleardates.get(counter).setFocus();
						}	
						
					}
					
					if(arg0.keyCode==SWT.ARROW_DOWN && counter < txtcleardates.size()-1 )
					{
						counter++;
						if(counter >= 0 && counter < txtcleardates.size())
						{	
							//System.out.println("hi..." +counter);
							txtcleardates.get(counter).setFocus();
						}
					}
					if(arg0.keyCode==SWT.ARROW_UP && counter >0)
					{
						counter--;
						if(counter >=0 && counter < txtcleardates.size())
						{	
							txtcleardates.get(counter).setFocus();
						}
					}
					
					/*if(arg0.keyCode==SWT.ARROW_UP && counter == 0)
					{
						btnviewclrunclr.setFocus();
					}*/
					
					
				}
			});
			
			txtmemos.get(clearcounter).addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//super.keyPressed(arg0);
					if(arg0.keyCode==SWT.ARROW_DOWN && counter < txtmemos.size()-1 )
					{
						counter++;
						if(counter >= 0 && counter < txtcleardates.size())
						{	
							txtmemos.get(counter).setFocus();
						}
					}
					if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
					{
						counter--;
						if(counter >= 0 && counter < txtmemos.size())
						{	
							txtmemos.get(counter).setFocus();
						}
					}
					
				}
			});
			
		}
		
		
		
		/*Btnclear.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
				{
					btnok.setFocus();
				}
			}
		});*/
		
		btnviewclrunclr.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				/*if(arg0.keyCode==SWT.ARROW_UP)
				{
					Btnclear.setFocus();
				}*/
				if(arg0.keyCode==SWT.ARROW_DOWN)
				{
					if(txtcleardates.size()>0)
					{
						txtcleardates.get(0).setFocus();
					}
				}
			}
		});
		
		
		
		btnbacktoviewrecon.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnRecon.setFocus();
					
				}
				
			}
		});
		
		
		btnRecon.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnbacktoviewrecon.setFocus();
					
				}
				
			}
		});
		
		btnRecon.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
	
				int reconCounter = 0;
				for (int counter = 0; counter < txtcleardates.size(); counter ++)
				{
					if(! txtcleardates.get(counter).getText().equals(""))
					{
						reconCounter ++;
					}
				}
				Object[][] reconData = new Object[reconCounter][7];
				int rowMarker = 0;
				MessageBox msgErrorDate = new MessageBox(new Shell(), SWT.OK | SWT.ICON_INFORMATION);
				msgErrorDate.setText("Information!");
				msgErrorDate.setMessage("Please enter a valid date in DD-MM-YYYY format");
				for(int fillrecon = 0; fillrecon< txtcleardates.size(); fillrecon ++)
				{
					/*if(txtcleardates.get(fillrecon).getText().trim().equals(""))
					{
						MessageBox msgErrorDate1 = new MessageBox(new Shell(), SWT.OK | SWT.ICON_INFORMATION);
						msgErrorDate1.setText("Information!");
						msgErrorDate1.setMessage("Nothing to Reconcile");
						txtcleardates.get(fillrecon).setFocus();
						msgErrorDate1.open();
						return;
					}*/
					if(! txtcleardates.get(fillrecon).getText().trim().equals(""))
					{
					if(txtcleardates.get(fillrecon).getText().length()!= 10 )
					{
						msgErrorDate.open();
						txtcleardates.get(fillrecon).setFocus();
						return;
					}
					
					
					if(txtcleardates.get(fillrecon).getText().substring(0,2).equals("00") &&  txtcleardates.get(fillrecon).getText().substring(3,5).equals("00") && txtcleardates.get(fillrecon).getText().substring(6).equals("0000"))
					{
						msgErrorDate.open();
						txtcleardates.get(fillrecon).setFocus();
						return;
					}
					
					if(! txtcleardates.get(fillrecon).getText().substring(2,3).equals("-") && ! txtcleardates.get(fillrecon).getText().substring(5,8).equals("-") )
					{
						msgErrorDate.open();
						txtcleardates.get(fillrecon).setFocus();
						return;
					}
					
						reconData[rowMarker][0] = voucherCodes.get(fillrecon);
						reconData[rowMarker][1] = refdates.get(fillrecon).substring(6) + "-" + refdates.get(fillrecon).substring(3,5)+"-"+refdates.get(fillrecon).substring(0,2); 
						reconData[rowMarker][2] =  accountnames.get(fillrecon);
						reconData[rowMarker][3] = dramounts.get(fillrecon);
						reconData[rowMarker][4] = cramounts.get(fillrecon);
						reconData[rowMarker][5] = txtcleardates.get(fillrecon).getText().substring(6)+"-"+txtcleardates.get(fillrecon).getText().substring(3,5)+"-"+txtcleardates.get(fillrecon).getText().substring(0,2);
						reconData[rowMarker][6] = txtmemos.get(fillrecon).getText();
						
						rowMarker ++;
						try {
							Date clearancedate = sdf.parse(txtcleardates.get(fillrecon).getText());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							MessageBox msg = new MessageBox(new Shell(), SWT.OK | SWT.ICON_INFORMATION);
							msg.setText("Error!");
							msg.setMessage("Invalid Date Format");
							txtcleardates.get(fillrecon).setFocus();
							msg.open();
							return;
						}
					}
				}
				
				Composite grandParent= (Composite)btnRecon.getParent().getParent();
				btnRecon.getParent().dispose();
				reportController.setReconcile(grandParent, reconData, bankname, FromDate, ToDate,strFromYear,projectName,narration1);
				}

		});

		
		btnviewclrunclr.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				Composite grandParent = (Composite) btnviewclrunclr.getParent().getParent();
				btnviewclrunclr.getParent().dispose();
				gnukhata.controllers.reportController.getClearedUnclearedTransactions(grandParent, bankname, FromDate, ToDate, projectName, narration1);
				/*clearedFlag = Btnclear.getSelection();
				if(clearedFlag==true)
				{
					btnok.getParent().dispose();
					//gnukhata.controllers.reportController.(grandParent,bankname, FromDate,ToDate, projectName, narration1, clearedFlag);
					gnukhata.controllers.reportController.getClearedUnclearedTransactions(grandParent, bankname, FromDate, ToDate, projectName, narration1, clearedFlag);
				}
				*/
			}
			
		});



		btnbacktoviewrecon.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnbacktoviewrecon.getParent().getParent();
				btnbacktoviewrecon.getParent().dispose();
					
					viewReconciliation vl=new viewReconciliation(grandParent,SWT.NONE);
					vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
		//ledgerReconReport.setFocus();
		}
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}

}
	

	